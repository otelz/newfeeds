﻿using OtelzNewFeeds.Common.Constants;
using OtelzNewFeeds.Common.Enums;
using OtelzNewFeeds.Models;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.OrmLite.Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace OtelzNewFeeds.Operations
{
    public interface ICacheOperations
    {
        void CacheAllProductsRTB(IDbConnection Db, ICacheClient Cache, bool clean = false);
        void CacheAllYandexProducts(IDbConnection Db, ICacheClient Cache, bool clean = false);
        void CacheAllGoogleProducts(IDbConnection Db, ICacheClient Cache, bool clean = false);
        void CacheAllYoutubeProducts(IDbConnection Db, ICacheClient Cache, bool clean = false);
        void CacheAllFacilities(IDbConnection Db, ICacheClient Cache, bool clean = false);
        void CacheAllRoutes(IDbConnection Db, ICacheClient Cache, RouteType type, byte restrictionType, byte facilityPublishStatus ,string lang, bool clean = false);
        void CacheAllProperties(IDbConnection Db, ICacheClient Cache, RouteType type,string lang, bool clean = false);
        void CacheFilteredProperties(IDbConnection Db, ICacheClient Cache, RouteType type,string lang, bool clean = false);
        void CacheAllFacebookPricelyProducts(IDbConnection Db, ICacheClient Cache, bool clean = false);
        void CacheAllFacilityFeedPrices(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, bool clean = false);
        void CacheAllFacilityFeedPricesForYandex(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, bool clean = false);
        void CacheMontlyAllFacilityFeedPrices(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, byte month, bool clean = false);
        void CacheFacilityImages(IDbConnection Db, ICacheClient Cache, int facilityId, bool clean = false);
    }
    public class CacheOperations : ICacheOperations
    {
        private static object _FacilityFeedPricesLockObj = new object();
        private static object _RTBAllProductsLockObj = new object();
        private static object _YandexProductsLockObj = new object();
        private static object _GoogleProductsLockObj = new object();
        private static object _YoutubeProductsLockObj = new object();
        private static object _RoutesLockObj = new object();
        private static object _PropertyLockObj = new object();
        private static object _FacebookPricelyProductsLockObj = new object();
        private static object _FacilitiesLockObj = new object();
        private static object _FacilitiyImagesLockObj = new object();
        public CacheOperations()
        {
            // Tell the container to inject dependancies
            HostContext.Container.AutoWire(this);
        }
        public void CacheAllProductsRTB(IDbConnection Db, ICacheClient Cache, bool clean = false)
        {
            var key = CacheKeys.RTBAllProducts;
            IEnumerable<RTBAllProducts_General> result = Cache.Get<List<RTBAllProducts_General>>(key);
            if (result == null || !result.Any() || clean)
                lock (_RTBAllProductsLockObj)
                {
                    result = Cache.Get<List<RTBAllProducts_General>>(key);
                    if (result == null)
                    {
                        result = Db.Query<RTBAllProducts_General>(@"[Integration].[RTBAllProducts_General]", new { Lang = "tr" }, commandType: CommandType.StoredProcedure);

                        result = RTBOperations.ApplyStringAdjusments(result);
                        if (result.Any())
                        {
                            Cache.Set(key, result, TimeSpan.FromHours(6));
                        }
                    }
                }
        }
        public void CacheAllYandexProducts(IDbConnection Db, ICacheClient Cache, bool clean = false)
        {
            var key = CacheKeys.YandexProducts;
            IEnumerable<YandexProducts_General> result = Cache.Get<List<YandexProducts_General>>(key);
            if (result == null || !result.Any() || clean)
                lock (_YandexProductsLockObj)
                {
                    result = Cache.Get<List<YandexProducts_General>>(key);
                    if (result == null)
                    {
                        result = Db.Query<YandexProducts_General>(@"[Integration].[YandexProducts_General]", new { Lang = "tr" }, commandType: CommandType.StoredProcedure);

                        result = YandexOperations.ApplyStringAdjusments(result);
                        if (result.Any())
                        {
                            Cache.Set(key, result, TimeSpan.FromHours(6));
                        }
                    }
                }
        }
        public void CacheAllGoogleProducts(IDbConnection Db, ICacheClient Cache, bool clean = false)
        {
            var key = CacheKeys.GoogleProducts;
            IEnumerable<GoogleProducts_General> result = Cache.Get<List<GoogleProducts_General>>(key);
            if (result == null || !result.Any() || clean)
                lock (_GoogleProductsLockObj)
                {
                    result = Cache.Get<List<GoogleProducts_General>>(key);
                    if (result == null)
                    {
                        result = Db.Query<GoogleProducts_General>(@"[Integration].[GoogleProducts_General]", new { Lang = "tr" }, commandType: CommandType.StoredProcedure);

                        result = GoogleOperations.ApplyStringAdjusments(result);
                        if (result.Any())
                        {
                            Cache.Set(key, result, TimeSpan.FromHours(6));
                        }
                    }

                }
        }
        public void CacheAllYoutubeProducts(IDbConnection Db, ICacheClient Cache, bool clean = false)
        {
            var key = CacheKeys.YoutubeProducts;
            IEnumerable<YoutubeProducts_General> result = Cache.Get<List<YoutubeProducts_General>>(key);
            if (result == null || !result.Any() || clean)
                lock (_YoutubeProductsLockObj)
                {
                    result = Cache.Get<List<YoutubeProducts_General>>(key);
                    if (result == null)
                    {
                        result = Db.Query<YoutubeProducts_General>(@"[Integration].[YoutubeProducts_General]", new { Lang = "tr" }, commandType: CommandType.StoredProcedure);

                        result = YoutubeOperations.ApplyStringAdjusments(result);
                        if (result.Any())
                        {
                            Cache.Set(key, result, TimeSpan.FromHours(6));
                        }
                    }

                }
        }
        public void CacheAllFacebookPricelyProducts(IDbConnection Db, ICacheClient Cache, bool clean = false)
        {
            var key = CacheKeys.FacebookPricelyProducts;
            IEnumerable<FacebookPricelyProducts_General> result = Cache.Get<List<FacebookPricelyProducts_General>>(key);
            if (result == null || !result.Any() || clean)
                lock (_FacebookPricelyProductsLockObj)
                {
                    result = Cache.Get<List<FacebookPricelyProducts_General>>(key);
                    if (result == null)
                    {

                        result = Db.Query<FacebookPricelyProducts_General>(@"[Integration].[FacebookPricelyProducts_General]", new { Lang = "tr" }, commandType: CommandType.StoredProcedure);

                        result = FacebookOperations.ApplyStringAdjusments(result);
                        if (result.Any())
                        {
                            Cache.Set(key, result, TimeSpan.FromHours(6));
                        }
                    }

                }
        }

        public void CacheAllFacilityFeedPrices(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, bool clean = false)
        {
            var key = CacheKeys.FacilityFeedPrices;
            IEnumerable<FacilityFeedPrices> result = Cache.Get<List<FacilityFeedPrices>>(key);
            if (result == null || !result.Any() || clean)
                lock (_FacilityFeedPricesLockObj)
                {
                    result = Cache.Get<List<FacilityFeedPrices>>(key);
                    if (result == null)
                    {
                        var param = new DynamicParameters();
                        param.Add("@CacheId", 0);
                        param.Add("@CheckIn", DateTime.Now.AddDays(1));
                        param.Add("@CheckOut", DateTime.Now.AddDays(2));
                        param.Add("@Device", 0);
                        param.Add("@Target", 0);
                        param.Add("@TargetId", 0);
                        param.Add("@ChannelId", 0);
                        param.Add("@CityId", 0);
                        param.Add("@UserCountry", "TR");
                        param.Add("@CountryCode", "TR");
                        param.Add("@MinRequiredRoom", 1);
                        param.Add("@TimezoneHour", 3);

                        DataTable dtRoom = new DataTable();
                        dtRoom.Columns.Add("RoomNo", typeof(byte));
                        dtRoom.Columns.Add("DataType", typeof(byte));
                        dtRoom.Columns.Add("Value", typeof(byte));

                        //Sırasıyla => RoomNo - DataType - Value
                        dtRoom.Rows.Add(1, 1, 2);
                        dtRoom.Rows.Add(1, 2, 0);

                        param.Add("@RoomOptions", dtRoom.AsTableValuedParameter("dbo.[SearchRoomsTable]"));

                        result = MemDb.Query<FacilityFeedPrices>(@"[dbo].[SearchResults_Select_ForCrawlerFull]", param, commandType: CommandType.StoredProcedure);
                        if (result.Any())
                        {
                            //CurrencyCommonOperations.GetStaticCrossRates(MemDb, Cache, true);
                            //CurrencyCommonOperations.GetOtelzCrossRatesDic(Db, Cache, true);
                            //result = FeedPriceOperations.ConvertAllPricesToTRY(result, Db, MemDb, Cache);
                            decimal staticCrossRate = 5.281m;
                            result.ToList().ForEach(x =>
                            {
                                x.Price = x.NetPrice * staticCrossRate;
                                x.SalePrice = x.NetPrice * staticCrossRate;
                            });
                            Cache.Set(key, result, TimeSpan.FromHours(3));
                        }
                    }

                }

        }

        public void CacheAllFacilityFeedPricesForYandex(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, bool clean = false)
        {
            var key = CacheKeys.FacilityFeedPricesYandex;
            IEnumerable<FacilityFeedPrices> result = Cache.Get<List<FacilityFeedPrices>>(key);
            if (result == null || !result.Any() || clean)
                lock (_FacilityFeedPricesLockObj)
                {
                    result = Cache.Get<List<FacilityFeedPrices>>(key);
                    if (result == null)
                    {
                        result = MemDb.Query<FacilityFeedPrices>(@"[dbo].[GetFacilityPricesForFeed]", commandType: CommandType.StoredProcedure);
                        if (result.Any())
                        {
                            CurrencyCommonOperations.GetStaticCrossRates(MemDb, Cache, true);
                            CurrencyCommonOperations.GetOtelzCrossRatesDic(Db, Cache, true);
                            result = FeedPriceOperations.ConvertAllPricesToRUB(result, Db, MemDb, Cache);
                            Cache.Set(key, result, TimeSpan.FromHours(3));
                        }
                    }
                }
        }

        public void CacheAllRoutes(IDbConnection Db, ICacheClient Cache, RouteType type, byte restrictionType, byte facilityPublishStatus, string lang,bool clean = false)
        {
            var key = CacheKeys.Routes + type.ToString() + '_' + restrictionType.ToString() + '_' + facilityPublishStatus.ToString() + '_' + lang;
            IEnumerable<Route_General> result = Cache.Get<List<Route_General>>(key);
            if (result == null || !result.Any() || clean)
                lock (_RoutesLockObj)
                {
                    result = Cache.Get<List<Route_General>>(key);
                    if (result == null)
                    {
                        result = Db.Query<Route_General>(@"[App].[RouteItems_ForFeeds_v2]", new { RouteType = type, RestrictionType = restrictionType, FacilityPublishStatus = facilityPublishStatus , Lang  = lang }, commandType: CommandType.StoredProcedure);

                        result = GoogleOperations.BuildPageUrl(result,lang);
                        if (result.Any())
                        {
                            Cache.Set(key, result, TimeSpan.FromHours(6));
                        }
                    }

                }
        }

        public void CacheAllProperties(IDbConnection Db, ICacheClient Cache, RouteType type, string lang, bool clean = false)
        {
            var key = CacheKeys.Properties + type.ToString() + '_' + lang;
            IEnumerable<Property_General> result = Cache.Get<List<Property_General>>(key);
            if (result == null || !result.Any() || clean)
            lock (_PropertyLockObj)
            {
                result = Cache.Get<List<Property_General>>(key);
                if (result == null)
                {
                    result = Db.Query<Property_General>(@"[App].[GetProperty_ForFeeds_Dev]", new { Lang = lang }, commandType: CommandType.StoredProcedure);

                    result = GoogleOperations.BuildPageUrl(result, lang);
                    if (result.Any())
                    {
                        Cache.Set(key, result, TimeSpan.FromHours(6));
                    }
                }
            }
        }

        public void CacheFilteredProperties(IDbConnection Db, ICacheClient Cache, RouteType type, string lang, bool clean = false)
        {
            var key = CacheKeys.FilteredProperties + type.ToString() + '_' + lang;
            IEnumerable<Property_General> result = Cache.Get<List<Property_General>>(key);
            if (result == null || !result.Any() || clean)
                lock (_PropertyLockObj)
                {
                    result = Cache.Get<List<Property_General>>(key);
                    if (result == null)
                    {
                        result = Db.Query<Property_General>(@"[App].[GetProperty_ForFeeds_24Hours]", new { Lang = lang }, commandType: CommandType.StoredProcedure);

                        result = GoogleOperations.BuildPageUrl(result, lang);
                        if (result.Any())
                        {
                            Cache.Set(key, result, TimeSpan.FromHours(6));
                        }
                    }
                }
        }

        public void CacheMontlyAllFacilityFeedPrices(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, byte month, bool clean = false)
        {
            var key = CacheKeys.FacilityFeedPrices + month;
            IEnumerable<FacilityFeedPrices> result = Cache.Get<List<FacilityFeedPrices>>(key);
            if (result == null || !result.Any() || clean)
                lock (_FacilityFeedPricesLockObj)
                {
                    result = Cache.Get<List<FacilityFeedPrices>>(key);
                    if (result == null)
                    {
                        result = MemDb.Query<FacilityFeedPrices>(@"[dbo].[GetFacilityPricesForFeedByMonth]", new { Month = month }, commandType: CommandType.StoredProcedure);
                        if (result.Any())
                        {
                            CurrencyCommonOperations.GetStaticCrossRates(MemDb, Cache, true);
                            CurrencyCommonOperations.GetOtelzCrossRatesDic(Db, Cache, true);
                            result = FeedPriceOperations.ConvertAllPricesToTRY(result, Db, MemDb, Cache);
                            Cache.Set(key, result, TimeSpan.FromHours(3));
                        }
                    }

                }
        }

        public void CacheAllFacilities(IDbConnection Db, ICacheClient Cache, bool clean = false)
        {
            var key = CacheKeys.Facilities;
            IEnumerable<Facility> result = Cache.Get<List<Facility>>(key);
            if (result == null || !result.Any() || clean)
                lock (_FacilitiesLockObj)
                {
                    result = Cache.Get<List<Facility>>(key);
                    if (result == null)
                    {
                        result = Db.Query<Facility>(@"[Integration].[GetFacilities]", commandType: CommandType.StoredProcedure);
                        if (result.Any())
                        {
                            Cache.Set(key, result, TimeSpan.FromHours(6));
                        }
                    }

                }
        }

        public void CacheFacilityImages(IDbConnection Db, ICacheClient Cache, int facilityId,  bool clean = false) 
        {
            var key = CacheKeys.FacilityImages + "_" + facilityId;
            IEnumerable<FacilityImages_General> result = Cache.Get<List<FacilityImages_General>>(key);
            if (result == null || !result.Any() || clean)
                lock (_RTBAllProductsLockObj)
                {
                    result = Cache.Get<List<FacilityImages_General>>(key);
                    if (result == null)
                    {
                        var param = new DynamicParameters();

                        param.Add("@Lang", "tr");
                        param.Add("@FacilityId", facilityId);

                        var sp = @"[Integration].[GetFacilityImages]";

                        result = Db.Query<FacilityImages_General>(sql: sp, param: param, commandType: CommandType.StoredProcedure);

                        result = FacilityImageOperations.ApplyStringAdjusments(result);
                        if (result.Any())
                        {
                            Cache.Set(key, result, TimeSpan.FromHours(1));
                        }
                    }
                }
        }
    }
}
