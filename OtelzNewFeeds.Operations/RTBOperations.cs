﻿using Flurl;
using OtelzNewFeeds.Common;
using OtelzNewFeeds.Common.Constants;
using OtelzNewFeeds.Common.Helpers;
using OtelzNewFeeds.Common.Models;
using OtelzNewFeeds.Models;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.OrmLite.Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace OtelzNewFeeds.Operations
{
    public interface IRTBOperations
    {
        List<RTBAllProducts> GetAllProductsRTB(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache);
        List<RTBAllProducts> GetMontlyAllProductsRTB(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, byte month);

    }

    public class RTBOperations : IRTBOperations
    {
        private static object _RTBAllProductsLockObj = new object();
        public IFeedPriceOperations FeedPriceOperations { get; set; }
        public ICacheOperations CacheOperations { get; set; }
        public RTBOperations()
        {
            // Tell the container to inject dependancies
            HostContext.Container.AutoWire(this);
        }
        public List<RTBAllProducts> GetAllProductsRTB(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache)
        {
            var key = CacheKeys.RTBAllProducts;
            var rtbAllProductsGeneral = Cache.Get<List<RTBAllProducts_General>>(key);
            if (rtbAllProductsGeneral == null)
            {
                CacheOperations.CacheAllProductsRTB(Db, Cache);
                rtbAllProductsGeneral = Cache.Get<List<RTBAllProducts_General>>(key);
            }

            var facilityCurrencyList = rtbAllProductsGeneral.Select(x => new FacilityCurrency
            {
                Currency = x.Currency,
                FacilityId = x.FacilityId
            }).ToList();

            var prices = FeedPriceOperations.GetFacilityFeedPrices(Db, MemDb, Cache, facilityCurrencyList);

            var allProductsRTB = BuidRTBAllProducts(rtbAllProductsGeneral, prices);

            return allProductsRTB;

        }
        private List<RTBAllProducts> BuidRTBAllProducts(List<RTBAllProducts_General> _Generals, IEnumerable<FacilityFeedPrices> _Prices)
        {
            return (from g in _Generals
                    join p in _Prices on g.FacilityId equals p.FacilityId
                    select new RTBAllProducts
                    {
                        PropertyID = g.FacilityId,
                        PropertyName = g.FacilityName,
                        FinalURL = g.FinalURL,
                        ImageURL = g.ImageURL,
                        DestinationName = g.DestinationName,
                        Description = g.AttributeDescription,
                        Star = g.StarNumber,
                        Price = CurrencyHelper.FormatCurrency_V2(p.Price, "TRY", 2),
                        SalePrice = CurrencyHelper.FormatCurrency_V2(p.SalePrice, "TRY", 2),
                        Category = g.Category,
                        ContextualKeywords = g.ContextualKeywords,
                        Adress = g.Address
                    }).ToList();

        }

        public static IEnumerable<RTBAllProducts_General> ApplyStringAdjusments(IEnumerable<RTBAllProducts_General> products_Generals)
        {
            products_Generals.ToList().ForEach(x =>
            {
                x.FinalURL = BuildFinalURL(x.FinalUrlSuffix);
                x.ImageURL = ImageHelper.BuildImageUrl(x.PhotoDirectory, x.PhotoName, ImageSizeItem.RSS);
                x.ContextualKeywords = BuildContextualKeywords(x.AddressKeys, x.FacilityName);
            });
            return products_Generals;
        }

        private static string BuildFinalURL(string finalUrlSuffix)
        {
            return $"https://www.otelz.com/hotel/{finalUrlSuffix}";
        }

        private static string BuildContextualKeywords(string addressKeys, string facilityName)
        {
            if (string.IsNullOrEmpty(addressKeys))
                return $"\"{facilityName}\"";

            string result = string.Empty;
            var addressKeysCleaned = addressKeys.Split(new char[] { ',' });
            foreach (var addressKey in addressKeysCleaned)
                if (addressKey.ToLower() != "merkez")
                    result += $"+{addressKey.Trim()} +{facilityName};";
            result += $"\"{facilityName}\";";
            return result;
        }

        public List<RTBAllProducts> GetMontlyAllProductsRTB(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, byte month)
        {
            var key = CacheKeys.RTBAllProducts;
            var rtbAllProductsGeneral = Cache.Get<List<RTBAllProducts_General>>(key);
            if (rtbAllProductsGeneral == null)
            {
                CacheOperations.CacheAllProductsRTB(Db, Cache);
                rtbAllProductsGeneral = Cache.Get<List<RTBAllProducts_General>>(key);
            }

            var facilityCurrencyList = rtbAllProductsGeneral.Select(x => new FacilityCurrency
            {
                Currency = x.Currency,
                FacilityId = x.FacilityId
            }).ToList();

            var prices = FeedPriceOperations.GetMontlyFacilityFeedPrices(Db, MemDb, Cache, facilityCurrencyList, month);

            var allProductsRTB = BuidRTBAllProducts(rtbAllProductsGeneral, prices);

            return allProductsRTB;
        }
    }
}
