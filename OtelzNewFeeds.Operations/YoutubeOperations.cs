﻿using OtelzNewFeeds.Common;
using OtelzNewFeeds.Common.Constants;
using OtelzNewFeeds.Common.Enums;
using OtelzNewFeeds.Common.Helpers;
using OtelzNewFeeds.Common.Models;
using OtelzNewFeeds.Models;
using ServiceStack;
using ServiceStack.Caching;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

namespace OtelzNewFeeds.Operations
{
    public interface IYoutubeOperations
    {
        List<YoutubeProduct> GetYoutubeProducts(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache);
    }

    public class YoutubeOperations : IYoutubeOperations
    {
        public IFeedPriceOperations FeedPriceOperations { get; set; }
        public ICacheOperations CacheOperations { get; set; }

        public AppConstants AppConstants { get; set; }


        public YoutubeOperations()
        {
            // Tell the container to inject dependancies
            HostContext.Container.AutoWire(this);
        }

        public List<YoutubeProduct> GetYoutubeProducts(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache)
        {
            var key = CacheKeys.YoutubeProducts;
            var youtubeProducts_Generals = Cache.Get<List<YoutubeProducts_General>>(key);
            if (youtubeProducts_Generals == null)
            {
                CacheOperations.CacheAllYoutubeProducts(Db, Cache);
                youtubeProducts_Generals = Cache.Get<List<YoutubeProducts_General>>(key);
            }

            var facilityCurrencyList = youtubeProducts_Generals.Select(x => new FacilityCurrency
            {
                Currency = x.Currency,
                FacilityId = x.FacilityId
            }).ToList();

            var prices = FeedPriceOperations.GetFacilityFeedPrices(Db, MemDb, Cache, facilityCurrencyList);

            var youtubeProducts = BuidYoutubeProducts(youtubeProducts_Generals, prices);

            return youtubeProducts;
        }
        private List<YoutubeProduct> BuidYoutubeProducts(List<YoutubeProducts_General> _Generals, IEnumerable<FacilityFeedPrices> _Prices)
        {
            return (from g in _Generals
                    join p in _Prices on g.FacilityId equals p.FacilityId
                    select new YoutubeProduct
                    {
                        PropertyId = g.FacilityId.ToString(),
                        PropertyName = g.FacilityName,
                        FinalUrl = g.FinalURL,
                        ImageUrl = g.ImageURL,
                        DestinationName = g.DestinationName,
                        Description = g.Description,
                        Price = Common.Helpers.CurrencyHelper.FormatCurrency_V2(p.Price, "TRY", 2),
                        SalePrice = Common.Helpers.CurrencyHelper.FormatCurrency_V2(p.SalePrice, "TRY", 2),
                        StarRating = g.StarNumber,
                        Category = g.Category,
                        ContextualKeywords = g.ContextualKeywords,
                        Address = g.Address,
                        FinalMobileUrl = g.FinalURL,
                        //AndroidAppLink = AppConstants.AndoridAppLink,
                        //IOSAppLink = AppConstants.IOSAppLink,
                        //IOSAppStoreId = AppConstants.IOSAppStoreId
                    }).ToList();

        }
        public static IEnumerable<YoutubeProducts_General> ApplyStringAdjusments(IEnumerable<YoutubeProducts_General> products_Generals)
        {
            products_Generals.ToList().ForEach(x =>
            {
                x.FinalURL = BuildFinalURL(x.FinalUrlSuffix);
                x.Description = $"{x.FacilityName} için en uygun fiyatları görüntüle.";
                x.ImageURL = ImageHelper.BuildImageUrl(x.PhotoDirectory, x.PhotoName, ImageSizeItem.Youtube);
                x.ContextualKeywords = BuildContextualKeywords(x.AddressKeys, x.FacilityName, x.Category);
                x.Address = CleanAddressString(x.Address);
            });
            return products_Generals;
        }

        private static string BuildFinalURL(string finalUrlSuffix)
        {
            return $"https://www.otelz.com/deeplinkgeneric/otel/{finalUrlSuffix}";
        }

        private static string BuildContextualKeywords(string addressKeys, string facilityName, string category)
        {
            if (string.IsNullOrEmpty(addressKeys))
                return $"\"{facilityName}\"";

            string result = string.Empty;
            var addressKeysCleaned = addressKeys.Split(new char[] { ',' });
            foreach (var addressKey in addressKeysCleaned)
                if (addressKey.ToLower() != "merkez")
                    result += $"{addressKey.Trim()};";
            result += $"{facilityName};";
            result += $"{category};";
            return result;
        }
        private static string CleanAddressString(string address)
        {
            if (!string.IsNullOrWhiteSpace(address))
            {
                var cleanedAddress = address;
                // \r\n karakterlerini ve gereksiz boşlukları temizleme
                while (cleanedAddress.Contains("\n") || cleanedAddress.Contains("\r"))
                {
                    cleanedAddress = address.Replace("\r\n", " ").Trim();
                    if (cleanedAddress.Contains("\n") || cleanedAddress.Contains("\r"))
                        cleanedAddress = address.Replace("\r", " ").Trim();
                    if (cleanedAddress.Contains("\n") || cleanedAddress.Contains("\r"))
                        cleanedAddress = address.Replace("\n", " ").Trim();
                }
                return cleanedAddress;
            }
            return address;
        }
    }
}
