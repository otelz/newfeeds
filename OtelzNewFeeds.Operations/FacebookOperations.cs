﻿using OtelzNewFeeds.Common;
using OtelzNewFeeds.Common.Constants;
using OtelzNewFeeds.Common.Helpers;
using OtelzNewFeeds.Common.Models;
using OtelzNewFeeds.Models;
using ServiceStack;
using ServiceStack.Caching;
using ServiceStack.OrmLite.Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace OtelzNewFeeds.Operations
{
    public interface IFacebookOperations
    {
        List<FacebookPricelyProducts> GetFacebookPricelyProducts(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, string filter);
        List<FacebookPricelyProducts> GetMontlyFacebookPricelyProducts(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, string filter, byte month);

    }
    public class FacebookOperations : IFacebookOperations
    {
        private static object _FacebookPricelyProductsLockObj = new object();

        public IFeedPriceOperations FeedPriceOperations { get; set; }
        public ICacheOperations CacheOperations { get; set; }

        public FacebookOperations()
        {
            // Tell the container to inject dependancies
            HostContext.Container.AutoWire(this);
        }      
        
        public List<FacebookPricelyProducts> GetFacebookPricelyProducts(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, string filter)
        {
            var key = CacheKeys.FacebookPricelyProducts;
            var facebookPricelyProductsGeneral = Cache.Get<List<FacebookPricelyProducts_General>>(key);
            if (facebookPricelyProductsGeneral == null)
            {
                CacheOperations.CacheAllFacebookPricelyProducts(Db, Cache);
                facebookPricelyProductsGeneral = Cache.Get<List<FacebookPricelyProducts_General>>(key);
            }

            if (!string.IsNullOrEmpty(filter))
                facebookPricelyProductsGeneral = facebookPricelyProductsGeneral.Where(x => x.FacilityName.Contains(filter)).ToList();

            var facilityCurrencyList = facebookPricelyProductsGeneral.Select(x => new FacilityCurrency
            {
                Currency = x.Currency,
                FacilityId = x.FacilityId
            }).ToList();

            var prices = FeedPriceOperations.GetFacilityFeedPrices(Db ,MemDb, Cache, facilityCurrencyList);

            var facebookPricelyProducts = BuidFacebookPricelyProducts(facebookPricelyProductsGeneral, prices);

            return facebookPricelyProducts;
        }
        public List<FacebookPricelyProducts> GetMontlyFacebookPricelyProducts(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, string filter, byte month)
        {
            var key = CacheKeys.FacebookPricelyProducts;
            var facebookPricelyProductsGeneral = Cache.Get<List<FacebookPricelyProducts_General>>(key);
            if (facebookPricelyProductsGeneral == null)
            {
                CacheOperations.CacheAllFacebookPricelyProducts(Db, Cache);
                facebookPricelyProductsGeneral = Cache.Get<List<FacebookPricelyProducts_General>>(key);
            }

            if (!string.IsNullOrEmpty(filter))
                facebookPricelyProductsGeneral = facebookPricelyProductsGeneral.Where(x => x.FacilityName.Contains(filter)).ToList();

            var facilityCurrencyList = facebookPricelyProductsGeneral.Select(x => new FacilityCurrency
            {
                Currency = x.Currency,
                FacilityId = x.FacilityId
            }).ToList();

            var prices = FeedPriceOperations.GetMontlyFacilityFeedPrices(Db, MemDb, Cache, facilityCurrencyList, month);

            var facebookPricelyProducts = BuidFacebookPricelyProducts(facebookPricelyProductsGeneral, prices);

            return facebookPricelyProducts;
        }
        private List<FacebookPricelyProducts> BuidFacebookPricelyProducts(List<FacebookPricelyProducts_General> _Generals, IEnumerable<FacilityFeedPrices> _Prices)
        {
            var facebookPricelyProducts = (from g in _Generals
                                           join p in _Prices on g.FacilityId equals p.FacilityId
                                           select new FacebookPricelyProducts
                                           {
                                               HotelId = g.FacilityId,
                                               Name = g.FacilityName,
                                               Url = g.FacilityUrl,
                                               Image = g.ImageUrl,
                                               Addresss = g.Address,
                                               Neighborhood = g.Neighborhood,
                                               Description = g.Description,
                                               Latitude = g.Latitude,
                                               Longitude = g.Longitude,
                                               Brand = g.Brand,
                                               CheckInDate = g.CheckInDate.ToString("yyyy-MM-dd"),
                                               Currency = g.Currency,
                                               LengthOfStay = g.LengthOfStay,
                                               BasePrice = CurrencyHelper.FormatCurrency(p.Price, "TRY", 2),
                                               SalePrice = CurrencyHelper.FormatCurrency(p.SalePrice, "TRY", 2),
                                               Fee = CurrencyHelper.FormatCurrency(g.Fee, "TRY", 1),
                                               Tax = CurrencyHelper.FormatCurrency(g.Tax, "TRY", 1),
                                               City = g.City,
                                               Region = g.Region,
                                               Country = g.Country
                                           }).ToList();
            return facebookPricelyProducts;

        }
        public static IEnumerable<FacebookPricelyProducts_General> ApplyStringAdjusments(IEnumerable<FacebookPricelyProducts_General> products_Generals)
        {
            products_Generals.ToList().ForEach(x =>
            {
                x.ImageUrl = ImageHelper.BuildImageUrl(x.PhotoDirectory, x.PhotoName, ImageSizeItem.Facebook);
                x.FacilityUrl = BuildFacilityUrl(x.FinalUrlSuffix);
                x.Address = BuildAddress(x.Country, x.City, x.Neighborhood);
            });
            return products_Generals;
        }

        private static string BuildFacilityUrl(string finalUrlSuffix)
        {
            return $"https://www.otelz.com/hotel/{finalUrlSuffix}";
        }

        private static string BuildAddress(string country, string city, string neighborhood)
        {
            return $"{country}-{city}-{neighborhood}";
        }
    }
}
