﻿using OtelzNewFeeds.Common.Constants;
using OtelzNewFeeds.Common.Enums;
using OtelzNewFeeds.Common.Helpers;
using OtelzNewFeeds.Common.Models;
using OtelzNewFeeds.Models;
using ServiceStack.Caching;
using ServiceStack.OrmLite.Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace OtelzNewFeeds.Operations
{
    public interface IFeedPriceOperations
    {
        IEnumerable<FacilityFeedPrices> GetFacilityFeedPrices(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache,List<FacilityCurrency> facilityCurrencies);
        IEnumerable<FacilityFeedPrices> GetFacilityFeedPricesForYandex(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache,List<FacilityCurrency> facilityCurrencies);
        IEnumerable<FacilityFeedPrices> GetMontlyFacilityFeedPrices(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, List<FacilityCurrency> facilityCurrencies, byte month);
    }
    public class FeedPriceOperations : IFeedPriceOperations
    {
        private static object _FacilityFeedPricesLockObj = new object();

        public ICacheOperations CacheOperations { get; set; }

        public FeedPriceOperations()
        {
            // Tell the container to inject dependancies
            ServiceStack.HostContext.Container.AutoWire(this);
        }
        public IEnumerable<FacilityFeedPrices> GetFacilityFeedPrices(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, List<FacilityCurrency> facilityCurrencies)
        {
            var key = CacheKeys.FacilityFeedPrices;
            var facilityFeedPrices = Cache.Get<IEnumerable<FacilityFeedPrices>>(key);

            if (facilityFeedPrices == null)
            {
                CacheOperations.CacheAllFacilityFeedPrices(Db, MemDb, Cache);
                facilityFeedPrices = Cache.Get<List<FacilityFeedPrices>>(key);
            }
            return facilityFeedPrices;
        }
        public IEnumerable<FacilityFeedPrices> GetFacilityFeedPricesForYandex(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, List<FacilityCurrency> facilityCurrencies)
        {
            var key = CacheKeys.FacilityFeedPricesYandex;
            var facilityFeedPrices = Cache.Get<IEnumerable<FacilityFeedPrices>>(key);

            if (facilityFeedPrices == null)
            {
                CacheOperations.CacheAllFacilityFeedPricesForYandex(Db, MemDb, Cache);
                facilityFeedPrices = Cache.Get<List<FacilityFeedPrices>>(key);
            }
            return facilityFeedPrices;
        }
        
        public IEnumerable<FacilityFeedPrices> GetMontlyFacilityFeedPrices(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, List<FacilityCurrency> facilityCurrencies, byte month)
        {
            var key = CacheKeys.FacilityFeedPrices + month;
            var facilityFeedPrices = Cache.Get<IEnumerable<FacilityFeedPrices>>(key);

            if (facilityFeedPrices == null)
            {
                CacheOperations.CacheMontlyAllFacilityFeedPrices(Db, MemDb, Cache, month);
                facilityFeedPrices = Cache.Get<List<FacilityFeedPrices>>(key);
            }
            return facilityFeedPrices;
        }
        public static IEnumerable<FacilityFeedPrices> ConvertAllPricesToTRY(IEnumerable<FacilityFeedPrices> facilityFeedPrices, IDbConnection Db, IDbConnection MemDb, ICacheClient Cache)
        {
            var staticRates = CurrencyCommonOperations.GetLiveRatesCrossStatic(MemDb, Cache);

            // First: Convert the price to the currency value of the facility.
            float rate;
            UnitType from;
            UnitType to = UnitType.TRY;
            FacilityCrossRate facilityCrossRate;
            facilityFeedPrices.ToList().ForEach(x =>
            {
                rate = (float)staticRates[x.Currency];
                from = x.Currency.ToEnum<UnitType>();
                facilityCrossRate = CurrencyCommonOperations.GetOtelzCrossRates(Db, Cache, from, to);
                x.Price = x.Price * (decimal)rate * facilityCrossRate.Rate;
                x.SalePrice = x.SalePrice * (decimal)rate * facilityCrossRate.Rate;
            });

            //// Second:  Convert the price to TL at the current rate.
            //UnitType from;
            //UnitType to = UnitType.TRY;
            //FacilityCrossRate facilityCrossRate;
            //facilityFeedPrices.ToList().ForEach(x =>
            //{
            //    from = x.Currency.ToEnum<UnitType>();
            //    facilityCrossRate = CurrencyCommonOperations.GetOtelzCrossRates(Db, Cache, from, to);

            //    x.Price = x.Price * (decimal)facilityCrossRate.Rate;
            //    x.SalePrice = x.SalePrice * (decimal)facilityCrossRate.Rate;
            //});
            return facilityFeedPrices;
        }

        public static IEnumerable<FacilityFeedPrices> ConvertAllPricesToRUB(IEnumerable<FacilityFeedPrices> facilityFeedPrices, IDbConnection Db, IDbConnection MemDb, ICacheClient Cache)
        {
            var staticRates = CurrencyCommonOperations.GetLiveRatesCrossStatic(MemDb, Cache);

            float rate;
            UnitType from;
            UnitType to = UnitType.RUB;
            FacilityCrossRate facilityCrossRate;
            facilityFeedPrices.ToList().ForEach(x =>
            {
                rate = (float)staticRates[x.Currency];
                from = x.Currency.ToEnum<UnitType>();
                facilityCrossRate = CurrencyCommonOperations.GetOtelzCrossRates(Db, Cache, from, to);
                x.Price = x.Price * (decimal)rate * facilityCrossRate.Rate;
                x.SalePrice = x.SalePrice * (decimal)rate * facilityCrossRate.Rate;
            });
            
            return facilityFeedPrices;
        }

    }
}
