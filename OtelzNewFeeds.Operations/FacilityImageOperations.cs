﻿using OtelzNewFeeds.Common;
using OtelzNewFeeds.Common.Constants;
using OtelzNewFeeds.Models;
using ServiceStack.Caching;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace OtelzNewFeeds.Operations
{
    public interface IFacilityImageOperations
    {
        List<FacilityImages> GetFacilityImages(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, int facilityId);
    }
    public class FacilityImageOperations : IFacilityImageOperations
    {
        private static object _RTBAllProductsLockObj = new object();
        public ICacheOperations CacheOperations { get; set; }
        
        public List<FacilityImages> GetFacilityImages(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, int facilityId)
        {
            if (facilityId == 0)
                return null;

            var key = CacheKeys.FacilityImages + "_" + facilityId;
            var facilityImagesGeneral = Cache.Get<List<FacilityImages_General>>(key);
            if (facilityImagesGeneral == null)
            {
                CacheOperations.CacheFacilityImages(Db, Cache, facilityId);
                facilityImagesGeneral = Cache.Get<List<FacilityImages_General>>(key);
            }
            var facilityImages = BuidFacilityImages(facilityImagesGeneral);

            return facilityImages;
        }

        private List<FacilityImages> BuidFacilityImages(List<FacilityImages_General> _Generals)
        {
            return (from g in _Generals
                    select new FacilityImages
                    {
                        PropertyID = g.FacilityId,
                        PropertyName = g.FacilityName,
                        ImageURL = g.ImageURL
                    }).ToList();
        }

        public static IEnumerable<FacilityImages_General> ApplyStringAdjusments(IEnumerable<FacilityImages_General> products_Generals)
        {
            products_Generals.ToList().ForEach(x =>
            {
                x.ImageURL = ImageHelper.BuildImageUrl(x.PhotoDirectory, x.PhotoName);
            });
            return products_Generals;
        }
    }
}
