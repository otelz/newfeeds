﻿using OtelzNewFeeds.Common;
using OtelzNewFeeds.Common.Constants;
using OtelzNewFeeds.Common.Enums;
using OtelzNewFeeds.Common.Helpers;
using OtelzNewFeeds.Common.Models;
using OtelzNewFeeds.Models;
using ServiceStack;
using ServiceStack.Caching;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

namespace OtelzNewFeeds.Operations
{
    public interface IYandexOperations
    {
        List<YandexProduct> GetYandexProducts(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache);
    }

    public class YandexOperations : IYandexOperations
    {
        public IFeedPriceOperations FeedPriceOperations { get; set; }
        public ICacheOperations CacheOperations { get; set; }
        public AppConstants AppConstants { get; set; }

        public List<YandexProduct> GetYandexProducts(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache)
        {
            var key = CacheKeys.YandexProducts;
            var yandexProducts_Generals = Cache.Get<List<YandexProducts_General>>(key);
            if (yandexProducts_Generals == null)
            {
                CacheOperations.CacheAllYandexProducts(Db, Cache);
                yandexProducts_Generals = Cache.Get<List<YandexProducts_General>>(key);
            }

            var facilityCurrencyList = yandexProducts_Generals.Select(x => new FacilityCurrency
            {
                Currency = x.Currency,
                FacilityId = x.FacilityId
            }).ToList();

            var prices = FeedPriceOperations.GetFacilityFeedPricesForYandex(Db, MemDb, Cache, facilityCurrencyList);

            var yandexProducts = BuildYandexProducts(yandexProducts_Generals, prices);

            return yandexProducts;
        }

        private List<YandexProduct> BuildYandexProducts(List<YandexProducts_General> _Generals, IEnumerable<FacilityFeedPrices> _Prices)
        {
            CultureInfo cultureInfo = new CultureInfo("en-US");
            return (from g in _Generals
                    join p in _Prices on g.FacilityId equals p.FacilityId
                    select new YandexProduct
                    {
                        PropertyID = g.FacilityId.ToString(),
                        PropertyName = g.FacilityName,
                        FinalURL = g.FinalURL,
                        ImageURL = g.ImageURL,
                        DestinationName = g.DestinationName,
                        Description = g.Description,
                        Price = string.Format("{0} RUB", p.Price.ToString("N2", cultureInfo)).ReplaceAll(",",""),
                        StarRating = int.TryParse(g.StarNumber, out int starNumberInt) ? starNumberInt > 0 ? g.StarNumber : "1" : "1",
                        Category = g.Category,
                        Address = g.Address,
                        Score = "",
                        MaxScore = "",
                        Facilities = ""
                    }).ToList();

        }

        public static IEnumerable<YandexProducts_General> ApplyStringAdjusments(IEnumerable<YandexProducts_General> products_Generals)
        {
            products_Generals.ToList().ForEach(x =>
            {
                x.FinalURL = BuildFinalURL(x.FinalUrlSuffix);
                x.ImageURL = ImageHelper.BuildImageUrl(x.PhotoDirectory, x.PhotoName, ImageSizeItem.Yandex);
                x.Address = CleanAddressString(x.Address);
            });
            return products_Generals;
        }

        private static string BuildFinalURL(string finalUrlSuffix)
        {
            return $"https://www.otelz.com/deeplinkgeneric/otel/{finalUrlSuffix}";
        }
        private static string CleanAddressString(string address)
        {
            if (!string.IsNullOrWhiteSpace(address))
            {
                var cleanedAddress = address;
                // \r\n karakterlerini ve gereksiz boşlukları temizleme
                while (cleanedAddress.Contains("\n") || cleanedAddress.Contains("\r"))
                {
                    cleanedAddress = address.Replace("\r\n", " ").Trim();
                    if (cleanedAddress.Contains("\n") || cleanedAddress.Contains("\r"))
                        cleanedAddress = address.Replace("\r", " ").Trim();
                        if (cleanedAddress.Contains("\n") || cleanedAddress.Contains("\r"))
                            cleanedAddress = address.Replace("\n", " ").Trim();
                }
                return cleanedAddress;
            }
            return address;
        }
    }

}
