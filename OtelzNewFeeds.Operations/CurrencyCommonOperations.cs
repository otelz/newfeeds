﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using ServiceStack.OrmLite.Dapper;
using ServiceStack.Caching;
using OtelzNewFeeds.Common.Models;
using OtelzNewFeeds.Common.Enums;
using OtelzNewFeeds.Common.Constants;

namespace OtelzNewFeeds.Operations
{
    public static class CurrencyCommonOperations
    {      

        public static decimal GetCrossRate(IDbConnection Db, ICacheClient Cache, string from, string to, int facilityId)
        {
            var dic = GetFacilityCrossRateDic(Db, Cache, facilityId);
            var key = from + "-" + to;
            if (dic.ContainsKey(key))
                return dic[key];
            else return 0;
        }


        public static FacilityCrossRate GetFacilityCrossRate(IDbConnection Db, ICacheClient Cache, int facilityId, UnitType from, UnitType to)
        {
            var result = new FacilityCrossRate()
            {
                ProviderId = -1000000,
                Rate = 1,
                FromCurrency = from,
                ToCurrency = to,
                Version = Guid.Empty,
                VersionDate = DateTime.UtcNow
            };
            if (from == to)
            {
                return result;
            }
            //TODO: Bunu DB'den getir.
            //var rates = CurrencyCommonOperations.GetCrossRatesDic(Cache);
            //var rate = rates[keyCurr];

            var rates = CurrencyCommonOperations.GetFacilityCrossRateDic(Db, Cache, facilityId);
            var keyCurr = result.FromCurrency.ToString() + "-" + result.ToCurrency.ToString();

            var rate = rates[keyCurr];

            result.Rate = rate;
            return result;
        }
        public static FacilityCrossRate GetOtelzCrossRates(IDbConnection Db, ICacheClient Cache, UnitType from, UnitType to)
        {
            var result = new FacilityCrossRate()
            {
                ProviderId = -1000000,
                Rate = 1,
                FromCurrency = from,
                ToCurrency = to,
                Version = Guid.Empty,
                VersionDate = DateTime.UtcNow
            };
            if (from == to)
            {
                return result;
            }
            //TODO: Bunu DB'den getir.
            //var rates = CurrencyCommonOperations.GetCrossRatesDic(Cache);
            //var rate = rates[keyCurr];

            var rates = CurrencyCommonOperations.GetOtelzCrossRatesDic(Db, Cache);
            var keyCurr = result.FromCurrency.ToString() + "-" + result.ToCurrency.ToString();

            var rate = rates[keyCurr];

            result.Rate = rate;
            return result;
        }
        public static Dictionary<string, decimal> GetFacilityCrossRateDic(IDbConnection Db, ICacheClient Cache, int facilityId)
        {

            var key = CacheKeys.GetFacilityCrossRateDic + facilityId;
            var dic = Cache.Get<Dictionary<string, decimal>>(key);
            if (dic == null || !dic.Any())
            {
                dic = new Dictionary<string, decimal>();
                var results = Db.Query<FacilityCrossRate>(sql: @"[App].[CrossRatesByFacility]", param: new { FacilityId = facilityId }, commandType: CommandType.StoredProcedure);
                if (results.Any())
                {
                    foreach (var item in results)
                    {
                        var dicKey = item.FromCurrency.ToString() + "-" + item.ToCurrency.ToString();
                        dic.Add(dicKey, item.Rate);
                    }
                    Cache.Set(key, dic, TimeSpan.FromHours(3));
                }
            }
            return dic;

        }
        public static Dictionary<string, decimal> GetOtelzCrossRatesDic(IDbConnection Db, ICacheClient Cache, bool clean = false)
        {

            var key = CacheKeys.GetOtelzCrossRatesDic;
            var dic = Cache.Get<Dictionary<string, decimal>>(key);
            if (dic == null || !dic.Any() || clean)
            {
                dic = new Dictionary<string, decimal>();
                var results = Db.Query<OtelzCrossRate>(sql: @"[App].[CrossRatesByProvier]", param: new { ProviderId = -1000 }, commandType: CommandType.StoredProcedure);
                if (results.Any())
                {
                    foreach (var item in results)
                    {
                        var dicKey = item.From.ToString() + "-" + item.To.ToString();
                        dic.Add(dicKey, item.Rate);
                    }
                    Cache.Set(key, dic, TimeSpan.FromHours(3));
                }
            }
            return dic;

        }
        public static IEnumerable<StaticCrossRate> GetStaticCrossRates(IDbConnection MemDb, ICacheClient Cache, bool clean = false)
        {
            var key = CacheKeys.StaticCrossRates;
            var results = Cache.Get<IEnumerable<StaticCrossRate>>(key);
            if (results == null || clean)
            {
                results = MemDb.Query<StaticCrossRate>(sql: @"[dbo].[GetStaticCrossRates]", commandType: CommandType.StoredProcedure);
                Cache.Set(key, results, TimeSpan.FromDays(7));
            }
            return results;
        }
        public static Dictionary<string, decimal> GetLiveRatesCrossStatic(IDbConnection MemDb, ICacheClient Cache, bool clean = false)
        {
            var staticRates = GetStaticCrossRates(MemDb, Cache, clean);
            // var codes = staticRates.Select(s=>s.ToCode);
            //var rates = CurrencyCommonOperations.GetCrossRatesDic(Cache);
            var dic = new Dictionary<string, decimal>();
            foreach (var sRate in staticRates)
            {
                dic.Add(sRate.ToCode, sRate.Rate);
            }
            return dic;
        }

        public static void GetAllCrossRate(IDbConnection Db, ICacheClient cache)
        {
            //TODO Duruma göre kullanılacak şuan için devre dışı
            var facilities = Db.Query<int>(sql: @"[Facility].[Facility_Select_ForCrossRate]", commandType: CommandType.StoredProcedure);
            if (facilities.Any())
            {
                foreach (var facilityId in facilities)
                {
                    GetFacilityCrossRateDic(Db, cache, facilityId);
                }
            }
        }
    }
}
