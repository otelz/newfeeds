﻿using OtelzNewFeeds.Common;
using OtelzNewFeeds.Common.Constants;
using OtelzNewFeeds.Common.Enums;
using OtelzNewFeeds.Common.Helpers;
using OtelzNewFeeds.Common.Models;
using OtelzNewFeeds.Models;
using ServiceStack;
using ServiceStack.Caching;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;

namespace OtelzNewFeeds.Operations
{
    public interface IGoogleOperations
    {
        List<GoogleProduct> GetGoogleProducts(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache);
        List<GoogleProduct> GetMontlyGoogleProducts(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, byte month);

        List<Route> GetRoutes(IDbConnection Db, ICacheClient Cache, RouteType type, byte restrictionType, byte facilityPublishStatus , string lang);
        List<OtelzNewFeeds.Models.Property> GetProperties(IDbConnection Db, ICacheClient Cache, RouteType type, string lang);
        List<OtelzNewFeeds.Models.Property> GetPropertiesLast24Hours(IDbConnection Db, ICacheClient Cache, RouteType type, string lang);

        List<Facility> GetFacilities(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache);

        List<Facility> GetHavingPricesFacilities(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache);

    }
    public class GoogleOperations : IGoogleOperations
    {
        public IFeedPriceOperations FeedPriceOperations { get; set; }
        public ICacheOperations CacheOperations { get; set; }

        public AppConstants AppConstants { get; set; }


        public GoogleOperations()
        {
            // Tell the container to inject dependancies
            HostContext.Container.AutoWire(this);
        }
        public List<GoogleProduct> GetGoogleProducts(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache)
        {
            var key = CacheKeys.GoogleProducts;
            var googleProducts_Generals = Cache.Get<List<GoogleProducts_General>>(key);
            if (googleProducts_Generals == null)
            {
                CacheOperations.CacheAllGoogleProducts(Db, Cache);
                googleProducts_Generals = Cache.Get<List<GoogleProducts_General>>(key);
            }

            var facilityCurrencyList = googleProducts_Generals.Select(x => new FacilityCurrency
            {
                Currency = x.Currency,
                FacilityId = x.FacilityId
            }).ToList();

            var prices = FeedPriceOperations.GetFacilityFeedPrices(Db, MemDb, Cache, facilityCurrencyList);

            var googleProducts = BuidGoogleProducts(googleProducts_Generals, prices);

            return googleProducts;
        }       
        private List<GoogleProduct> BuidGoogleProducts(List<GoogleProducts_General> _Generals, IEnumerable<FacilityFeedPrices> _Prices)
        {
            return (from g in _Generals
                    join p in _Prices on g.FacilityId equals p.FacilityId
                    select new GoogleProduct
                    {
                        PropertyId = g.FacilityId.ToString(),
                        PropertyName = g.FacilityName,                        
                        FinalUrl = g.FinalURL,
                        ImageUrl = g.ImageURL,
                        DestinationName = g.DestinationName,
                        Description = g.Description,
                        Price = Common.Helpers.CurrencyHelper.FormatCurrency_V2(p.Price, "TRY", 2),
                        SalePrice = Common.Helpers.CurrencyHelper.FormatCurrency_V2(p.SalePrice, "TRY", 2),
                        StarRating = g.StarNumber,                       
                        Category = g.Category,
                        ContextualKeywords = g.ContextualKeywords,
                        Address = g.Address,
                        FinalMobileUrl = g.FinalURL
                        //AndroidAppLink = AppConstants.AndoridAppLink,
                        //IOSAppLink = AppConstants.IOSAppLink,
                        //IOSAppStoreId = AppConstants.IOSAppStoreId
                    }).ToList();

        }
        public static IEnumerable<GoogleProducts_General> ApplyStringAdjusments(IEnumerable<GoogleProducts_General> products_Generals)
        {
            products_Generals.ToList().ForEach(x =>
            {
                x.FinalURL = BuildFinalURL(x.FinalUrlSuffix);
                x.Description = $"{x.FacilityName} için en uygun fiyatları görüntüle.";
                x.ImageURL = ImageHelper.BuildImageUrl(x.PhotoDirectory, x.PhotoName, ImageSizeItem.Google);
                x.ContextualKeywords = BuildContextualKeywords(x.AddressKeys, x.FacilityName, x.Category);
            });
            return products_Generals;
        }

        private static string BuildFinalURL(string finalUrlSuffix)
        {
            return $"https://www.otelz.com/deeplinkgeneric/otel/{finalUrlSuffix}";
        }

        private static string BuildContextualKeywords(string addressKeys, string facilityName, string category)
        {
            if (string.IsNullOrEmpty(addressKeys))
                return $"\"{facilityName}\"";

            string result = string.Empty;
            var addressKeysCleaned = addressKeys.Split(new char[] { ',' });
            foreach (var addressKey in addressKeysCleaned)
                if (addressKey.ToLower() != "merkez")
                    result += $"{addressKey.Trim()};";
            result += $"{facilityName};";
            result += $"{category};";
            return result;
        }

        public List<Route> GetRoutes(IDbConnection Db, ICacheClient Cache, RouteType type, byte restrictionType , byte facilityPublishStatus , string lang)
        {
            var key = CacheKeys.Routes + type.ToString() + '_' + restrictionType.ToString() + '_' + facilityPublishStatus.ToString() + '_' + lang;
            var routes_general = Cache.Get<List<Route_General>>(key);
            if (routes_general == null)
            {
                CacheOperations.CacheAllRoutes(Db, Cache, type, restrictionType, facilityPublishStatus , lang);
                routes_general = Cache.Get<List<Route_General>>(key);
            }

            var routes = BuildRoutes(routes_general);

            return routes;
        }

        public List<OtelzNewFeeds.Models.Property> GetProperties(IDbConnection Db, ICacheClient Cache, RouteType type, string lang)
        {
            var key = CacheKeys.Properties + type.ToString() + '_' + lang;
            var property_general = Cache.Get<List<Property_General>>(key);
            if (property_general == null)
            {
                CacheOperations.CacheAllProperties(Db, Cache, type, lang);
                property_general = Cache.Get<List<Property_General>>(key);
            }

            var property = BuildProperties(property_general);

            return property;
        }
        public List<OtelzNewFeeds.Models.Property> GetPropertiesLast24Hours(IDbConnection Db, ICacheClient Cache, RouteType type, string lang)
        {
            var key = CacheKeys.FilteredProperties + type.ToString() + '_' + lang;
            var property_general = Cache.Get<List<Property_General>>(key);
            if (property_general == null)
            {
                CacheOperations.CacheFilteredProperties(Db, Cache, type, lang);
                property_general = Cache.Get<List<Property_General>>(key);
            }

            List<OtelzNewFeeds.Models.Property> properties = new List<OtelzNewFeeds.Models.Property>();

            if (property_general != null && property_general.Count > 0)
            {
                properties = BuildProperties(property_general);
            }

            return properties;
        }

        
        private static List<Route> BuildRoutes(IEnumerable<Route_General> routes)
        {
            return (from r in routes
                    select new Route { PageURL = r.PageURL }).ToList();
        }

        private static List<OtelzNewFeeds.Models.Property> BuildProperties(IEnumerable<Property_General> properties)
        {
            return (from p in properties    
                    select new OtelzNewFeeds.Models.Property 
                    { 
                        PageURL = p.PageURL,
                        Action = p.Action,
                        PropertyType =  StringHelper.RemoveTurkish(p.PropertyType.ToLower()),
                    }).ToList();
        }
        public static IEnumerable<Route_General> BuildPageUrl(IEnumerable<Route_General> routes , string lang = "tr")
        {
            string root = "https://www.otelz.com/";
            string url = string.Empty;
            routes.ToList().ForEach(route =>
            {
                var urlSuffix = GetUrlSuffix(route.RouteType, lang);
                url = route.Name + urlSuffix;

                if (route.RouteType == (short)RouteType.Facility)
                {
                    //url = "deeplinkgeneric/hotel/" + url;
                    url = "hotel/" + url;
                }
                route.PageURL = root + url;
            });
            return routes;
        }

        public static IEnumerable<Property_General> BuildPageUrl(IEnumerable<Property_General> properties, string lang = "tr")
        {
            string root = "https://www.otelz.com/";
            string url = string.Empty;
            properties.ToList().ForEach(property =>
            {
                var urlSuffix = GetUrlSuffix(property.RouteType, lang);
                url = property.Name + urlSuffix;

                if (property.RouteType == (short)RouteType.Facility)
                {
                    url = "hotel/" + url;
                }
                property.PageURL = root + url;
            });
            return properties;
        }

        private static object GetUrlSuffix(short routeType, string lang)
        {
            var suffixLabel = "";   
            if (routeType == (short)RouteType.Place)
            {
                suffixLabel = "-otelleri";
                switch (lang)
                {
                    case "en":
                        suffixLabel = "-hotels?lang=en";
                        break;
                    case "de":
                        suffixLabel = "-hotels?lang=de";
                        break;
                    case "it":
                        suffixLabel = "-hotels?lang=it";
                        break;
                    case "ru":
                        suffixLabel = "-hotels?lang=ru";
                        break;
                    case "fr":
                        suffixLabel = "-hotels?lang=fr";
                        break;
                    case "es":
                        suffixLabel = "-hotels?lang=es";
                        break;
                    default:
                        break;
                }
            }
            else if (routeType == (short)RouteType.Facility)
            {
                switch (lang)
                {
                    case "en":
                        suffixLabel = "?lang=en";
                        break;
                    case "de":
                        suffixLabel = "?lang=de";
                        break;
                    case "it":
                        suffixLabel = "?lang=it";
                        break;
                    case "ru":
                        suffixLabel = "?lang=ru";
                        break;
                    case "fr":
                        suffixLabel = "?lang=fr";
                        break;
                    case "es":
                        suffixLabel = "?lang=es";
                        break;
                    default:
                        break;
                }
            }


            return suffixLabel;
        }

        public List<GoogleProduct> GetMontlyGoogleProducts(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache, byte month)
        {
            var key = CacheKeys.GoogleProducts;
            var googleProducts_Generals = Cache.Get<List<GoogleProducts_General>>(key);
            if (googleProducts_Generals == null)
            {
                CacheOperations.CacheAllGoogleProducts(Db, Cache);
                googleProducts_Generals = Cache.Get<List<GoogleProducts_General>>(key);
            }

            var facilityCurrencyList = googleProducts_Generals.Select(x => new FacilityCurrency
            {
                Currency = x.Currency,
                FacilityId = x.FacilityId
            }).ToList();

            var prices = FeedPriceOperations.GetMontlyFacilityFeedPrices(Db, MemDb, Cache, facilityCurrencyList, month);

            var googleProducts = BuidGoogleProducts(googleProducts_Generals, prices);

            return googleProducts;
        }

        public List<Facility> GetFacilities(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache)
        {
            var key = CacheKeys.Facilities;
            var facilities = Cache.Get<List<Facility>>(key);
            if (facilities == null)
            {
                CacheOperations.CacheAllFacilities(Db, Cache);
                facilities = Cache.Get<List<Facility>>(key);
            }
            return facilities;
        }

        public List<Facility> GetHavingPricesFacilities(IDbConnection Db, IDbConnection MemDb, ICacheClient Cache)
        {
            var key = CacheKeys.Facilities;
            var facilities = Cache.Get<List<Facility>>(key);
            if (facilities == null)
            {
                CacheOperations.CacheAllFacilities(Db, Cache);
                facilities = Cache.Get<List<Facility>>(key);
            }

            var facilityCurrencyList = facilities.Select(x => new FacilityCurrency
            {
                Currency = "TRY",
                FacilityId = x.FacilityId
            }).ToList();

            var prices = FeedPriceOperations.GetFacilityFeedPrices(Db, MemDb, Cache, facilityCurrencyList);

            return (from f in facilities
                    join p in prices on f.FacilityId equals p.FacilityId
                    select new Facility
                    {
                        FacilityId = f.FacilityId,
                        GlobalName = f.GlobalName,
                        PublishStatus = f.PublishStatus
                    }).ToList();
        }
    }
}
