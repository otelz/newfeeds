﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace OtelzNewFeeds.ServiceModel
{


    #region request

    [Route("/GetYandexProducts")]
    public class GetYandexProducts
    {

    }

    #endregion


    #region response

    [DataContract]
    public class YandexProduct
    {
        [DataMember(Name = "Property ID")]
        public string PropertyID { get; set; }
        [DataMember(Name = "Property name")]
        public string PropertyName { get; set; }
        [DataMember(Name = "Final URL")]
        public string FinalURL { get; set; }
        [DataMember(Name = "Image URL")]
        public string ImageURL { get; set; }
        [DataMember(Name = "Destination name")]
        public string DestinationName { get; set; }
        [DataMember(Name = "Description")]
        public string Description { get; set; }
        [DataMember(Name = "Price")]
        public string Price { get; set; }
        [DataMember(Name = "Star rating")]
        public string StarRating { get; set; }
        [DataMember(Name = "Category")]
        public string Category { get; set; }
        [DataMember(Name = "Address")]
        public string Address { get; set; }
        [DataMember(Name = "Score")]
        public string Score { get; set; }
        [DataMember(Name = "Max score")]
        public string MaxScore { get; set; }
        [DataMember(Name = "Facilities")]
        public string Facilities { get; set; }
    }
    #endregion
}
