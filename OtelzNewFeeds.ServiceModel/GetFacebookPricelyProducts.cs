﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace OtelzNewFeeds.ServiceModel
{
    #region request

    [Route("/GetFacebookPricelyProducts")]
    [Route("/GetFacebookPricelyProducts/{filter}")]
    public class GetFacebookPricelyProducts
    {
        public string filter { get; set; }
    }
    [Route("/GetMontlyFacebookPricelyProducts/{Month}")]
    [Route("/GetMontlyFacebookPricelyProducts/{filter}/{Month}")]
    public class GetMontlyFacebookPricelyProducts
    {
        public string filter { get; set; }
        public byte Month { get; set; }
    }
    #endregion

    #region response
    [XmlRoot(ElementName = "component")]
    public class Component
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "address")]
    public class Address
    {
        [XmlElement(ElementName = "component")]
        public List<Component> Component { get; set; }
        [XmlAttribute(AttributeName = "format")]
        public string Format { get; set; }
    }

    [XmlRoot(ElementName = "image")]
    public class Image
    {
        [XmlElement(ElementName = "url")]
        public string Url { get; set; }
    }

    [XmlRoot(ElementName = "listing")]
    public class Listing
    {
        [XmlElement(ElementName = "hotel_id")]
        public string HotelId { get; set; }
        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "url")]
        public string Url { get; set; }
        [XmlElement(ElementName = "brand")]
        public string Brand { get; set; }
        [XmlElement(ElementName = "address")]
        public Address Address { get; set; }
        [XmlElement(ElementName = "image")]
        public Image Image { get; set; }
        [XmlElement(ElementName = "latitude")]
        public string Latitude { get; set; }
        [XmlElement(ElementName = "longitude")]
        public string Longitude { get; set; }
        [XmlElement(ElementName = "neighborhood")]
        public string Neighborhood { get; set; }
        [XmlElement(ElementName = "sale_price")]
        public string SalePrice { get; set; }
        [XmlElement(ElementName = "base_price")]
        public string BasePrice { get; set; }
    }

    [XmlRoot(ElementName = "listings")]
    public class Listings
    {
        [XmlElement(ElementName = "title")]
        public string Title { get; set; }
        [XmlElement(ElementName = "listing")]
        public List<Listing> Listing { get; set; }
    }
    #endregion
}
