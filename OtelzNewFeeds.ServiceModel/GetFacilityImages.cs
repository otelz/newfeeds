﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace OtelzNewFeeds.ServiceModel
{
    [Route("/GetFacilityImages/{FacilityId}")]
    public class GetFacilityImages
    {
        public int FacilityId { get; set; }
    }


    [DataContract(Name = "FacilityImages")]
    public class FacilityImages
    {
        [DataMember(Name = "PropertyID")]
        public string PropertyID { get; set; }

        [DataMember(Name = "PropertyName")]
        public string PropertyName { get; set; }

        [DataMember(Name = "ImageURL")]
        public string ImageURL { get; set; }
    }

    [CollectionDataContract(Name = "FacilityImageList")]
    public class FacilityImageList : List<FacilityImages>
    {

    }

}
