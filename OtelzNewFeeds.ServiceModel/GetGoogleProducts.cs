﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace OtelzNewFeeds.ServiceModel
{
    #region request
    [Route("/GetGoogleProducts")]
    public class GetGoogleProducts
    {

    }
    [Route("/GetMontlyGoogleProducts/{Month}")]
    public class GetMontlyGoogleProducts
    {
        public byte Month { get; set; }
    }
    #endregion


    #region response

    [DataContract]
    public class GoogleProduct
    {
        [DataMember(Name = "Property ID")]
        public string PropertyId { get; set; }
        [DataMember(Name = "Property name")]
        public string PropertyName { get; set; }
        [DataMember(Name = "Final URL")]
        public string FinalUrl { get; set; }
        [DataMember(Name = "Image URL")]
        public string ImageUrl { get; set; }
        [DataMember(Name = "Destination name")]
        public string DestinationName { get; set; }
        [DataMember(Name = "Description")]
        public string Description { get; set; }
        [DataMember(Name = "Price")]
        public string Price { get; set; }
        [DataMember(Name = "Sale price")]
        public string SalePrice { get; set; }
        [DataMember(Name = "Star rating")]
        public string StarRating { get; set; }
        [DataMember(Name = "Category")]
        public string Category { get; set; }
        [DataMember(Name = "Contextual keywords")]
        public string ContextualKeywords { get; set; }
        [DataMember(Name = "Address")]
        public string Address { get; set; }
        [DataMember(Name = "Tracking template")]
        public string TrackingTemplate { get; set; }
        [DataMember(Name = "Custom parameter")]
        public string CustomParameter { get; set; }
        [DataMember(Name = "Final mobile URL")]
        public string FinalMobileUrl { get; set; }
        [DataMember(Name = "Android app link")]
        public string AndroidAppLink { get; set; }
        [DataMember(Name = "iOS app link")]
        public string IOSAppLink { get; set; }
        [DataMember(Name = "iOS app store ID")]
        public string IOSAppStoreId { get; set; }

    }

    #endregion
}
