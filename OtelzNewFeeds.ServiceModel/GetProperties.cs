﻿using OtelzNewFeeds.Common.Enums;
using ServiceStack;
using System.Runtime.Serialization;

namespace OtelzNewFeeds.ServiceModel
{
    [Route("/GetProperty")]
    [Route("/GetProperty/{Lang}")]
    public class GetProperties
    {
        public string Lang { get; set; }
        public RouteType RouteType { get; set; } = RouteType.Facility; //Şimdilik sadece tesis için
    }
    [Route("/GetProperty-24")]
    [Route("/GetProperty-24/{Lang}")]
    public class GetProperties24
    {
        public string Lang { get; set; }
        public RouteType RouteType { get; set; } = RouteType.Facility; //Şimdilik sadece tesis için
    }

    [DataContract]
    public class Property
    {
        [DataMember(Name = "Page URL")]
        public string PageURL { get; set; }
        [DataMember(Name = "Custom label")]
        public string PropertyType { get; set; }
        [DataMember(Name = "Action")]
        public string Action { get; set; }
    }
}
