﻿using OtelzNewFeeds.Common.Enums;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace OtelzNewFeeds.ServiceModel
{
    [Route("/GetFacilities")]
    public class GetFacilities
    {
        
    }
    [Route("/GetHavingPricesFacilities")]
    public class GetHavingPricesFacilities
    {

    }

    [DataContract]
    public class Facility
    {
        [DataMember(Name = "FacilityId")]
        public int FacilityId { get; set; }
        [DataMember(Name = "GlobalName")]
        public string GlobalName { get; set; }
        [DataMember(Name = "PublishStatus")]
        public byte PublishStatus { get; set; }
    }
        
}
