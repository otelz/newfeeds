﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace OtelzNewFeeds.ServiceModel
{
    [Route("/GetAllProductsReTargeting")]    
    public class GetAllProductsReTargeting
    {
       
    }
    [Route("/GetMontlyAllProductsReTargeting/{Month}")]
    public class GetMontlyAllProductsReTargeting
    {
        public byte Month { get; set; }
    }

    [DataContract(Name = "product")]
    public class Product
    {
        [DataMember(Name = "PropertyID")]
        public string PropertyID { get; set; }

        [DataMember(Name = "PropertyName")]
        public string PropertyName { get; set; }

        [DataMember(Name = "FinalURL")]
        public string FinalURL { get; set; }

        [DataMember(Name = "ImageURL")]
        public string ImageURL { get; set; }

        [DataMember(Name = "Star")]
        public string Star { get; set; }

        [DataMember(Name = "DestinationName")]
        public string DestinationName { get; set; }

        [DataMember(Name = "Description")]
        public string Description { get; set; }

        [DataMember(Name = "Price")]
        public string Price { get; set; }

        [DataMember(Name = "SalePrice")]
        public string SalePrice { get; set; }

        [DataMember(Name = "Category")]
        public string Category { get; set; }

        [DataMember(Name = "ContextualKeywords")]
        public string ContextualKeywords { get; set; }

        [DataMember(Name = "Adress")]
        public string Adress { get; set; }
    }

    [CollectionDataContract(Name = "ProductList")]
    public class ProductList: List<Product> 
    {        
        
    }

}
