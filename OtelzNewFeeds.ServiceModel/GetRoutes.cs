﻿using OtelzNewFeeds.Common.Enums;
using ServiceStack;
using System.Runtime.Serialization;

namespace OtelzNewFeeds.ServiceModel
{
    [Route("/GetRoutes/{RouteType}")]
    [Route("/GetRoutes/{RouteType}/{FacilityPublishStatus}")]
    [Route("/GetRoutes/{RouteType}/{FacilityPublishStatus}/{RestrictionType}")]
    public class GetRoutes
    {
        public RouteType RouteType { get; set; }
        public byte FacilityPublishStatus { get; set; } = 1;
        public byte RestrictionType { get; set; } = 0;
        public string Lang { get; set; }
    }

    [DataContract]
    public class Route
    {
        [DataMember(Name = "Page URL")]
        public string PageURL { get; set; }
    }
        
}
