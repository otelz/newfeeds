﻿using ServiceStack;
using OtelzNewFeeds.ServiceModel;
using OtelzNewFeeds.Operations;
using System.Collections.Generic;

namespace OtelzNewFeeds.ServiceInterface
{
    public class YandexService : ServiceBase
    {
        public IYandexOperations YandexOperations { get; set; }

        [AddHeader(ContentType = "text/csv;charset=UTF-8")]
        [AddHeader(ContentDisposition = "content-disposition", Value = "attachment;filename=YandexProducts.csv")]
        public List<YandexProduct> Any(GetYandexProducts request)
        {
            var yandexProducts = YandexOperations.GetYandexProducts(Db, MemDb, Cache);

            var productsDto = yandexProducts.ConvertTo<List<YandexProduct>>();

            return productsDto;
        }
    }
}
