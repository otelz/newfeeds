﻿using OtelzNewFeeds.Common.Factory;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace OtelzNewFeeds.ServiceInterface
{
    public class ServiceBase : Service
    {
        public ServiceBase()
        {
            // Tell the container to inject dependancies
            HostContext.Container.AutoWire(this);
        }
        public virtual IMemoryOptimizedConnectionFactory MemDbFactory
        {
            get { return TryResolve<IMemoryOptimizedConnectionFactory>(); }
        }
        private IDbConnection memDb;
        public virtual IDbConnection MemDb
        {
            get { return memDb ?? (memDb = MemDbFactory.OpenDbConnection()); }
        }
        public override void Dispose()
        {
            base.Dispose();
            if (memDb != null)
                memDb.Dispose();
        }
    }
}
