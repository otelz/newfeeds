using ServiceStack;
using OtelzNewFeeds.ServiceModel;
using OtelzNewFeeds.Operations;
using System.Collections.Generic;

namespace OtelzNewFeeds.ServiceInterface
{
    public class FacebookFeedsService : ServiceBase
    {
        public IFacebookOperations FacebookOperations { get; set; }        

        [AddHeader(ContentType = "application/text")]
        [AddHeader(ContentDisposition = "content-disposition",Value = "attachment;filename=FacebookPricelyProducts.xml")]
        public Listings Any(GetFacebookPricelyProducts request)
        {
            var facebookPricelyProducts = FacebookOperations.GetFacebookPricelyProducts(Db, MemDb, Cache, request.filter);

            var listings = new Listings();
            listings.Title = "Otelz Hotel Feed";

            List<Listing> listingArray = new List<Listing>();

            foreach (var facebook in facebookPricelyProducts)
            {
                var listing = new Listing();
                listing.HotelId = facebook.HotelId.ToString();
                listing.Name = facebook.Name;
                listing.Description = facebook.Description;
                listing.Url = facebook.Url;
                listing.Brand = facebook.Brand;
                listing.Address = new Address
                {
                    Format = "simple",
                    Component = new List<Component>
                    {
                        new Component
                        {
                            Name="addr1",
                            Text = facebook.Addresss
                        },
                        new Component
                        {
                            Name="city",
                            Text =facebook.City
                        },
                        new Component
                        {
                            Name="region",
                            Text=facebook.Region
                        },
                        new Component
                        {
                            Name ="country",
                            Text =facebook.Country
                        }
                    }
                };
                listing.Image = new Image
                {
                    Url = facebook.Image
                };
                listing.Latitude = facebook.Latitude.ToString();
                listing.Longitude = facebook.Longitude.ToString();
                listing.Neighborhood = facebook.Neighborhood;
                listing.SalePrice = facebook.SalePrice;
                listing.BasePrice = facebook.BasePrice;

                listingArray.Add(listing);
            }
            listings.Listing = listingArray;

            return listings;
        }
        [AddHeader(ContentType = "application/text")]
        [AddHeader(ContentDisposition = "content-disposition", Value = "attachment;filename=FacebookPricelyProducts.xml")]
        public Listings Any(GetMontlyFacebookPricelyProducts request)
        {
            var facebookPricelyProducts = FacebookOperations.GetMontlyFacebookPricelyProducts(Db, MemDb, Cache, request.filter, request.Month);

            var listings = new Listings();
            listings.Title = "Otelz Hotel Feed";

            List<Listing> listingArray = new List<Listing>();

            foreach (var facebook in facebookPricelyProducts)
            {
                var listing = new Listing();
                listing.HotelId = facebook.HotelId.ToString();
                listing.Name = facebook.Name;
                listing.Description = facebook.Description;
                listing.Url = facebook.Url;
                listing.Brand = facebook.Brand;
                listing.Address = new Address
                {
                    Format = "simple",
                    Component = new List<Component>
                    {
                        new Component
                        {
                            Name="addr1",
                            Text = facebook.Addresss
                        },
                        new Component
                        {
                            Name="city",
                            Text =facebook.City
                        },
                        new Component
                        {
                            Name="region",
                            Text=facebook.Region
                        },
                        new Component
                        {
                            Name ="country",
                            Text =facebook.Country
                        }
                    }
                };
                listing.Image = new Image
                {
                    Url = facebook.Image
                };
                listing.Latitude = facebook.Latitude.ToString();
                listing.Longitude = facebook.Longitude.ToString();
                listing.Neighborhood = facebook.Neighborhood;
                listing.SalePrice = facebook.SalePrice;
                listing.BasePrice = facebook.BasePrice;

                listingArray.Add(listing);
            }
            listings.Listing = listingArray;

            return listings;
        }
    }
}
