﻿using OtelzNewFeeds.Operations;
using OtelzNewFeeds.ServiceModel;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OtelzNewFeeds.ServiceInterface
{
    public class FacilityImageService : ServiceBase
    {
        public IFacilityImageOperations FacilityImageOperations { get; set; }

        [AddHeader(ContentType = MimeTypes.XmlText)]
        public object Any(GetFacilityImages request)
        {
            var facilityImages = FacilityImageOperations.GetFacilityImages(Db, MemDb, Cache, request.FacilityId);
            
            var imageUrl = string.Empty;
            if (facilityImages != null)
            {
                imageUrl = facilityImages.FirstOrDefault()?.ImageURL;
            }

            if (!string.IsNullOrWhiteSpace(imageUrl))
            {
                Response.Redirect(imageUrl);
                return imageUrl;
            }
            else
            {
                // Görüntü bulunamadıysa normal işleminize devam edin
                var images = facilityImages.ConvertTo<FacilityImageList>();
                return images;
            }
        }
    }
}
