﻿using OtelzNewFeeds.Operations;
using OtelzNewFeeds.ServiceModel;
using ServiceStack;
using System.Collections.Generic;

namespace OtelzNewFeeds.ServiceInterface
{
    public class YoutubeService : ServiceBase
    {
        public IYoutubeOperations YoutubeOperations { get; set; }

        [AddHeader(ContentType = "text/csv;charset=UTF-8")]
        [AddHeader(ContentDisposition = "content-disposition", Value = "attachment;filename=YoutubeProducts.csv")]
        public List<YoutubeProduct> Any(GetYoutubeProducts request)
        {
            var youtubeProducts = YoutubeOperations.GetYoutubeProducts(Db, MemDb, Cache);

            var productsDto = youtubeProducts.ConvertTo<List<YoutubeProduct>>();

            return productsDto;
        }
    }
}
