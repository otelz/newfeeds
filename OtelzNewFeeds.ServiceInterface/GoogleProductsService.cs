﻿using OtelzNewFeeds.Operations;
using OtelzNewFeeds.ServiceModel;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.ServiceInterface
{
    public class GoogleProductsService : ServiceBase
    {
        public IGoogleOperations GoogleOperations { get; set; }

        public GoogleProductsService()
        {

        }

        [AddHeader(ContentType = "text/csv;charset=UTF-8")]
        [AddHeader(ContentDisposition = "content-disposition", Value = "attachment;filename=GoogleProducts.csv")]
        public List<GoogleProduct> Any(GetGoogleProducts request)
        {
            var googleProducts = GoogleOperations.GetGoogleProducts(Db, MemDb, Cache);

            var productsDto = googleProducts.ConvertTo<List<GoogleProduct>>();

            return productsDto;
        }
        [AddHeader(ContentType = "text/csv;charset=UTF-8")]
        [AddHeader(ContentDisposition = "content-disposition", Value = "attachment;filename=GoogleProducts.csv")]
        public List<GoogleProduct> Any(GetMontlyGoogleProducts request)
        {
            var googleProducts = GoogleOperations.GetMontlyGoogleProducts(Db, MemDb, Cache, request.Month);

            var productsDto = googleProducts.ConvertTo<List<GoogleProduct>>();

            return productsDto;
        }
        [AddHeader(ContentType = "text/csv;charset=UTF-8")]
        [AddHeader(ContentDisposition = "content-disposition", Value = "attachment;filename=Routes.csv")]
        public List<Route> Any(GetRoutes request)
        {
            request.Lang = request.Lang ?? "tr";

            var routes = GoogleOperations.GetRoutes(Db,Cache, request.RouteType, request.RestrictionType, request.FacilityPublishStatus , request.Lang);

            var routesDto = routes.ConvertTo<List<Route>>();

            return routesDto;
        }
        [AddHeader(ContentType = "text/csv;charset=UTF-8")]
        [AddHeader(ContentDisposition = "content-disposition", Value = "attachment;filename=Properties.csv")]
        public List<ServiceModel.Property> Any(GetProperties request)
        {
            request.Lang = request.Lang ?? "tr";

            var properties = GoogleOperations.GetProperties(Db, Cache, request.RouteType, request.Lang);

            var propertyDto = properties.ConvertTo<List<ServiceModel.Property>>();

            return propertyDto;
        }
        [AddHeader(ContentType = "text/csv;charset=UTF-8")]
        [AddHeader(ContentDisposition = "content-disposition", Value = "attachment;filename=PropertiesLast24Hours.csv")]
        public List<ServiceModel.Property> Any(GetProperties24 request)
        {
            request.Lang = request.Lang ?? "tr";

            var properties = GoogleOperations.GetPropertiesLast24Hours(Db, Cache, request.RouteType, request.Lang);

            var propertyDto = properties.ConvertTo<List<ServiceModel.Property>>();

            return propertyDto;
        }
        
        [AddHeader(ContentType = "text/csv;charset=UTF-8")]
        [AddHeader(ContentDisposition = "content-disposition", Value = "attachment;filename=Facilities.csv")]
        public List<Facility> Any(GetFacilities request)
        {
            var facilities = GoogleOperations.GetFacilities(Db, MemDb, Cache);

            var facilitiesDto = facilities.ConvertTo<List<Facility>>();

            return facilitiesDto;
        }
        [AddHeader(ContentType = "text/csv;charset=UTF-8")]
        [AddHeader(ContentDisposition = "content-disposition", Value = "attachment;filename=HavingPricesFacilities.csv")]
        public List<Facility> Any(GetHavingPricesFacilities request)
        {
            var facilities = GoogleOperations.GetHavingPricesFacilities(Db, MemDb, Cache);

            var facilitiesDto = facilities.ConvertTo<List<Facility>>();

            return facilitiesDto;
        }
    }
}
