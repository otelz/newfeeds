using ServiceStack;
using OtelzNewFeeds.ServiceModel;
using OtelzNewFeeds.Operations;
using System.Collections.Generic;

namespace OtelzNewFeeds.ServiceInterface
{
    public class RTBService : ServiceBase
    {
        public IRTBOperations RTBOperations { get; set; }

        [AddHeader(ContentType = MimeTypes.XmlText)]
        public object Any(GetAllProductsReTargeting request)
        {
            var rtbProducts = RTBOperations.GetAllProductsRTB(Db, MemDb, Cache);

            var productsDto = rtbProducts.ConvertTo<ProductList>();
            
            return productsDto;
        }
        [AddHeader(ContentType = MimeTypes.XmlText)]
        public object Any(GetMontlyAllProductsReTargeting request)
        {
            var rtbProducts = RTBOperations.GetMontlyAllProductsRTB(Db, MemDb, Cache, request.Month);

            var productsDto = rtbProducts.ConvertTo<ProductList>();

            return productsDto;
        }
    }
}
