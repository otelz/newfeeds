﻿using OtelzNewFeeds.Operations;
using OtelzNewFeeds.ServiceModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.ServiceInterface
{
    public class CacheService : ServiceBase
    {

        public ICacheOperations CacheOperations { get; set; }

        public string Any(ResetFacilityFeedPrices request)
        {
            CacheOperations.CacheAllFacilityFeedPrices(Db, MemDb, Cache,true);
            return "true";
        }
        public string Any(ResetFacebookPricelyProducts request)
        {
            CacheOperations.CacheAllFacebookPricelyProducts(Db, Cache, true);
            return "true";
        }
        public string Any(ResetProductsRTB request)
        {
            CacheOperations.CacheAllProductsRTB(Db, Cache, true);
            return "true";
        }
    }
}
