﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Common.Constants
{
    public class AppConstants
    {
        public string AndoridAppLink { get; set; }
        public string IOSAppLink { get; set; }
        public string IOSAppStoreId { get; set; }
    }
}
