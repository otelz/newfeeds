﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Common.Constants
{
    public class CacheKeys
    {
        public const string RTBAllProducts = "RTBAllProducts";
        public const string YandexProducts = "YandexProducts";
        public const string GoogleProducts = "GoogleProducts";
        public const string YoutubeProducts = "YoutubeProducts";
        public const string Facilities = "Facilities";
        public const string Routes = "Routes";
        public const string Properties = "Properties";
        public const string FilteredProperties = "FilteredProperties";
        public const string FacebookPricelyProducts = "FacebookPricelyProducts";
        public const string FacilityFeedPrices = "FacilityFeedPrices";
        public const string FacilityFeedPricesYandex = "FacilityFeedPricesYandex";
        public const string GetFacilityCrossRateDic = "GetFacilityCrossRateDic-";
        public const string GetOtelzCrossRatesDic = "GetOtelzCrossRatesDic";
        public const string StaticCrossRates = "StaticCrossRates";
        public const string FacilityImages = "FacilityImages";

    }
}
