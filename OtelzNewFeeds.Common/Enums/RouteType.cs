﻿using OtelzNewFeeds.Common.Helpers.Attributes;
using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Common.Enums
{
    [EnumAsInt, NameAttribute("Url Tipleri"), Description("Url Tipleri"), TypeId(572)]
    public enum RouteType 
    {
        [NameAttribute("Tanımsız"), Description("Tanımsız")]
        Undefined = 0,

        [NameAttribute("Lokasyon"), Description("Lokasyon")]
        Place = 1,

        [NameAttribute("Tesis"), Description("Tesis")]
        Facility = 2,

        [NameAttribute("Dinamic Arama"), Description("Dinamic Arama")]
        DynamicSearch = 3,

        [NameAttribute("Statik Sayfa"), Description("Statik Sayfa")]
        StaticPage = 4,
        [NameAttribute("Özel Landing"), Description("Özel Landing")]
        SpecialLanding = 5,
    }
}
