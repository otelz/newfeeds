﻿using OtelzNewFeeds.Common.Helpers.Attributes;
using ServiceStack.DataAnnotations;

namespace OtelzNewFeeds.Common.Enums
{
    [NameAttribute("Birimler & Para Birimleri"), Description("Birimler & Para Birimleri"), TypeId(710)]
    [EnumAsInt]
    public enum UnitType : byte
    {
        //UP TO 100 ARE CURRENCIES
        [NameAttribute("Bilinmiyor"), SymbolAttribute("Bilinmiyor")]
        XXX = 0,

        [NameAttribute("Türk Lirası"), SymbolAttribute("TL")]
        TRY = 1,

        [NameAttribute("Dolar"), SymbolAttribute("$")]
        USD = 2,

        [NameAttribute("EURO"), SymbolAttribute("€")]
        EUR = 3,

        [NameAttribute("İngiliz Sterlini"), SymbolAttribute("£")]
        GBP = 4,

        [NameAttribute("Avustralya Doları"), SymbolAttribute("$")]
        AUD = 5,

        [NameAttribute("Danimarka Kronu"), SymbolAttribute("")]
        DKK = 6,

        [NameAttribute("İsviçre Frangı"), SymbolAttribute("")]
        CHF = 7,

        [NameAttribute("İsveç Kronu"), SymbolAttribute("")]
        SEK = 8,

        [NameAttribute("Kanada Doları"), SymbolAttribute("")]
        CAD = 9,
        [NameAttribute("Kuveyt Dinarı"), SymbolAttribute("")]
        KWD = 10,
        [NameAttribute("Norveç Kronu"), SymbolAttribute("")]
        NOK = 11,
        [NameAttribute("Suudi Arabistan Riyali"), SymbolAttribute("")]
        SAR = 12,
        [NameAttribute("Japon Yeni"), SymbolAttribute("")]
        JPY = 13,
        [NameAttribute("Bulgar Levası"), SymbolAttribute("")]
        BGN = 14,
        [NameAttribute("Rumen Leyi"), SymbolAttribute("")]
        RON = 15,
        [NameAttribute("Rus Rublesi"), SymbolAttribute("")]
        RUB = 16,
        [NameAttribute("İran Riyali"), SymbolAttribute("")]
        IRR = 17,
        [NameAttribute("Çin Yuanı"), SymbolAttribute("")]
        CNY = 18,
        [NameAttribute("Katar Riyali"), SymbolAttribute("")]
        QAR = 19,
        [NameAttribute("Özel Çekme Hakları"), SymbolAttribute("")]
        XDR = 20,
        [NameAttribute("Pakistan Riyali"), SymbolAttribute("")]
        PKR = 21,

        //UP TO 100 ARE CURRENCIES

        [NameAttribute("Metre"), SymbolAttribute("m")]
        Meter = 101,

        [NameAttribute("Kilometre"), SymbolAttribute("km")]
        Kilometer = 102,

        [NameAttribute("Santigrat"), SymbolAttribute("°C")]
        Centigrade = 103,

        [NameAttribute("Yüzde"), SymbolAttribute("%")]
        Percent = 104,

        [NameAttribute("Adet"), SymbolAttribute("Adet")]
        Unit = 105,

    }
}
