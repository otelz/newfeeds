﻿using OtelzNewFeeds.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Common.Models
{
    public class FacilityCrossRate
    {
        public virtual int ProviderId { get; set; }
        public virtual UnitType FromCurrency { get; set; }
        public virtual UnitType ToCurrency { get; set; }
        public virtual Guid Version { get; set; }
        public virtual decimal Rate { get; set; }
        public virtual byte RateType { get; set; }
        public virtual DateTime VersionDate { get; set; }

    }
}
