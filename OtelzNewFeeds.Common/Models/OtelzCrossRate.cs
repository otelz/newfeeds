﻿using OtelzNewFeeds.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Common.Models
{
    public class OtelzCrossRate
    {
        public virtual UnitType From { get; set; }
        public virtual UnitType To { get; set; }
        public virtual Guid Version { get; set; }
        public virtual decimal Rate { get; set; }

    }
}
