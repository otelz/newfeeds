﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Common.Models
{
    public class StaticCrossRate
    {
        public virtual byte FromId { get; set; }
        public virtual string FromCode { get; set; }
        public virtual byte ToId { get; set; }
        public virtual string ToCode { get; set; }
        public virtual decimal Rate { get; set; }

        public virtual decimal LiveRate { get; set; }

    }
}
