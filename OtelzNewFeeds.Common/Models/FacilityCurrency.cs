﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Common.Models
{
    public class FacilityCurrency
    {
        public int FacilityId { get; set; }
        public string Currency { get; set; }
    }
}
