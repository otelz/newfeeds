﻿using System;
using System.Linq;

namespace OtelzNewFeeds.Common
{
    public class ImageSizeItem : IEquatable<ImageSizeItem>
    {
        public static ImageSizeItem Empty => new ImageSizeItem("");
        public static ImageSizeItem RSS => new ImageSizeItem("w-450,h-800");
        public static ImageSizeItem Facebook => new ImageSizeItem("w-600,h-600");
        public static ImageSizeItem Google => new ImageSizeItem("w-450,h-800");
        public static ImageSizeItem Yandex => new ImageSizeItem("w-450,h-800");
        public static ImageSizeItem Youtube => new ImageSizeItem("w-300,h-300");

        public static ImageSizeItem[] SupportedItems => new[] { RSS, Facebook, Google, Yandex, Youtube };

        public static bool IsSupported(ImageSizeItem item) => SupportedItems.Any(l => l == item);

        public static bool IsSupported(string name) => SupportedItems.Any(l => l == name);

        public string Size { get; }

        public ImageSizeItem(string size)
        {
            Size = size;
        }

        public bool Equals(ImageSizeItem other)
        {
            return object.Equals(Size, other.Size);
        }

        public override bool Equals(object obj)
        {
            var other = obj as ImageSizeItem;
            if (other == null)
            {
                return false;
            }
            else
            {
                return Equals(other);
            }
        }

        public override int GetHashCode()
        {
            return Size.GetHashCode();
        }

        public override string ToString()
        {
            return Size;
        }

        public static implicit operator string(ImageSizeItem obj)
        {
            return obj.Size;
        }

        public static bool operator ==(ImageSizeItem x, ImageSizeItem y)
        {
            return x.Equals(y);
        }

        public static bool operator !=(ImageSizeItem x, ImageSizeItem y)
        {
            return !x.Equals(y);
        }
    }
}
