﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace OtelzNewFeeds.Common.Helpers
{
    public static class StringHelper
    {
        public static string RemoveTurkish(this string text)
        {
            if (string.IsNullOrEmpty(text))
                return "";
            var response = String.Join("", text.Normalize(NormalizationForm.FormD)
                         .Where(c => char.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark))
                         .Replace("İ", "i").Replace("ı", "i").Replace("ş", "s").Replace("Ş", "s")
                         .Replace("ü", "u").Replace("Ü", "U").Replace("Ç", "c").Replace("ç", "c")
                         .Replace("ğ", "g").Replace("Ğ", "g").Replace("ö", "o").Replace("Ö", "o")
                         .Replace("'", "").Replace("&", "").Replace("\"", "");
            return Regex.Replace(response, "[^0-9a-zA-Z] +", "");
        }
    }
}
