﻿using Flurl;
using System.Collections.Generic;

namespace OtelzNewFeeds.Common
{
    public static class ImageHelper
    {
        public static string BuildImageUrl(string directory, string name, ImageSizeItem size)
        {
            var urlPaths = new List<string>
            {
                "https://imgkit.otelz.com/",
                directory,
                name,
                $"?tr={size},fo-auto"                
            };

            return Url.Combine(urlPaths.ToArray());
        }

        public static string BuildImageUrl(string directory, string name)
        {
            var urlPaths = new List<string>
            {
                "https://imgkit.otelz.com/",
                directory,
                name,
                "?tr=fo-auto"
            };

            return Url.Combine(urlPaths.ToArray());
        }
    }
}
