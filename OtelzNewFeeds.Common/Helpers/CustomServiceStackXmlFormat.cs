﻿using ServiceStack.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OtelzNewFeeds.Common.Helpers
{
    public static class CustomServiceStackXmlFormat
    {
        public static string Format = "application/text";

        public static void Serialize(IRequest req, object response, Stream stream)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(response.GetType());
            xmlSerializer.Serialize(stream, response);
        }
        public static void Serialize2(IRequest req, object response, Stream stream)
        {
            var serializer = new XmlSerializer(response.GetType());
            using (var xw = XmlWriter.Create(stream))
            {
                serializer.Serialize(xw, response);
            }
        }
        public static object Deserialize(Type type, Stream stream)
        {
            byte[] xmlBytes = null;
            stream.Position = 0;
            using (var newMemoryStream = new MemoryStream())
            {
                stream.CopyTo(newMemoryStream);
                newMemoryStream.Position = 0;
                using (var binaryReader = new BinaryReader(newMemoryStream))
                {
                    xmlBytes = binaryReader.ReadBytes((int)newMemoryStream.Length);
                }
            }
            string xmlString = Encoding.UTF8.GetString(xmlBytes);
            var serializer = new XmlSerializer(type);
            object result;

            using (TextReader reader = new StringReader(xmlString))
            {
                result = serializer.Deserialize(reader);
            }
            return result;
        }
        public static object DeserializeServiceStack(Type type, Stream stream)
        {
            var serializer = new System.Runtime.Serialization.DataContractSerializer(type);
            return serializer.ReadObject(stream);
        }
    }
}
