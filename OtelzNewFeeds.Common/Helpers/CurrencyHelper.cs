﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace OtelzNewFeeds.Common.Helpers
{
    public class CurrencyHelper
    {
        // Lookup dictionary of CultureInfo by currency ISO code (e.g  USD, GBP, JPY)
        static Dictionary<string, CultureInfo> _currencyCultureInfo;

        static CurrencyHelper()
        {
            _currencyCultureInfo = new Dictionary<string, CultureInfo>();

            // get the list of cultures. We are not interested in neutral cultures, since
            // currency and RegionInfo is only applicable to specific cultures
            var _cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

            foreach (var ci in _cultures)
            {
                // Create a RegionInfo from culture id. 
                // RegionInfo holds the currency ISO code
                try
                {
                    var ri = new RegionInfo(ci.LCID);

                    // multiple cultures can have the same currency code
                    if (!_currencyCultureInfo.ContainsKey(ri.ISOCurrencySymbol))
                        _currencyCultureInfo.Add(ri.ISOCurrencySymbol, ci);

                }
                catch (ArgumentException) { }
            }
        }
        static public CultureInfo CultureInfoFromCurrencyISO(string isoCode)
        {
            if (_currencyCultureInfo.ContainsKey(isoCode))
                return _currencyCultureInfo[isoCode];
            else
                return null;
        }
        static public string GetCurrencyFormat(string currencyISO)
        {
            var testResult = FormatCurrency(1, currencyISO, 0);
            return testResult.Replace("1", "{0}");
        }
        static public string FormatCurrency(decimal amount, string currencyISO)
        {
            return FormatCurrency(amount, currencyISO, 0);
        }
        static public string FormatCurrency(float amount, string currencyISO)
        {
            return FormatCurrency((decimal)amount, currencyISO, 0);
        }
        static public string FormatCurrency(decimal amount, string currencyISO, int digit)
        {

            
            if (amount <= 0.1m)
                return "0";
            if (digit > 0 && (amount - (Math.Floor(amount)) < 0.1m))
                digit = 0;
            var c = CultureInfoFromCurrencyISO(currencyISO);
            if (currencyISO == "TRY")
            {
                c = CultureInfo.CreateSpecificCulture("tr-TR");
                c.NumberFormat.CurrencySymbol = "";
                c.NumberFormat.CurrencyDecimalDigits = digit;
                c.NumberFormat.CurrencyDecimalSeparator = ",";
                c.NumberFormat.CurrencyGroupSeparator = ".";
                return String.Format("{0} {1}", amount.ToString("C", c.NumberFormat), "TRY");
            }
            else if (c == null)
            {

                c = CultureInfo.CreateSpecificCulture("tr-TR");
                c.NumberFormat.CurrencySymbol = "";
                c.NumberFormat.CurrencyDecimalDigits = digit;
                c.NumberFormat.CurrencyDecimalSeparator = ",";
                c.NumberFormat.CurrencyGroupSeparator = ".";

                return String.Format("{0} {1}", currencyISO, amount.ToString("C", c.NumberFormat));
            }
            else
            {
                c.NumberFormat.CurrencyDecimalDigits = digit;
                c.NumberFormat.CurrencyDecimalSeparator = ",";
                c.NumberFormat.CurrencyGroupSeparator = ".";
                if (currencyISO == "TRY")
                    c.NumberFormat.CurrencySymbol = "TRY";
                return amount.ToString("C", c.NumberFormat);
            }

        }

        /// <summary>
        /// NumberFormat
        /// CurrencyDecimalSeparator = "."
        /// CurrencyGroupSeparator = ""
        /// </summary>
        static public string FormatCurrency_V2(decimal amount, string currencyISO, int digit)
        {
            if (amount <= 0.1m)
                return "0";
            if (digit > 0 && (amount - (Math.Floor(amount)) < 0.1m))
                digit = 0;
            var c = CultureInfoFromCurrencyISO(currencyISO);
            if (currencyISO == "TRY")
            {
                c = CultureInfo.CreateSpecificCulture("tr-TR");
                c.NumberFormat.CurrencySymbol = "";
                c.NumberFormat.CurrencyDecimalDigits = digit;
                c.NumberFormat.CurrencyDecimalSeparator = ".";
                c.NumberFormat.CurrencyGroupSeparator = "";
                return String.Format("{0} {1}", amount.ToString("C", c.NumberFormat), "TRY");
            }
            else if (c == null)
            {

                c = CultureInfo.CreateSpecificCulture("tr-TR");
                c.NumberFormat.CurrencySymbol = "";
                c.NumberFormat.CurrencyDecimalDigits = digit;
                c.NumberFormat.CurrencyDecimalSeparator = ".";
                c.NumberFormat.CurrencyGroupSeparator = "";

                return String.Format("{0} {1}", currencyISO, amount.ToString("C", c.NumberFormat));
            }
            else
            {
                c.NumberFormat.CurrencyDecimalDigits = digit;
                c.NumberFormat.CurrencyDecimalSeparator = ".";
                c.NumberFormat.CurrencyGroupSeparator = "";
                if (currencyISO == "TRY")
                    c.NumberFormat.CurrencySymbol = "TRY";
                return amount.ToString("C", c.NumberFormat);
            }

        }
    }
}
