﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Common.Helpers.Attributes
{
    [AttributeUsage(AttributeTargets.All)]
    public class NameAttribute : Attribute
    {
        public string Name { get; set; }
        public NameAttribute(string name)
        {
            this.Name = name;
        }
    }
    [AttributeUsage(AttributeTargets.All)]
    public class SymbolAttribute : Attribute
    {
        public string Name { get; set; }
        public SymbolAttribute(string name)
        {
            this.Name = name;
        }
    }
    [AttributeUsage(AttributeTargets.All)]
    public class TypeId : Attribute
    {
        public short Id { get; set; }
        public TypeId(short id)
        {
            this.Id = id;
        }
    }
}
