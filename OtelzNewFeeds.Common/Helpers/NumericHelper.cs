﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Common.Helpers
{
    public static class NumericHelper
    {
        public static decimal AddDecimals(decimal value, int decimalPlaces)
        {
            var divideBy = Math.Pow(10, decimalPlaces);
            return (decimal)(value / (decimal)divideBy);
        }

        public static decimal LoseDecimals(decimal value, int decimalPlaces)
        {
            var multiplyWith = Math.Pow(10, decimalPlaces);
            return (decimal)((int)(value * (decimal)multiplyWith));
        }
    }
}
