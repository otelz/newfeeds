﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Common.Helpers
{
    public static class EnumHelper
    {
        /// <summary>
        /// Extension method to return an enum value of type T for the given string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
        /// <summary>
        /// Extension method to return an enum value of type T for the given int.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ToEnum<T>(this int value)
        {
            var name = Enum.GetName(typeof(T), value);
            return name.ToEnum<T>();
        }
        public static byte ConvertToByte<T>(string name, byte defaultValue = 0)
        {
            byte result = defaultValue;
            try
            {
                result = (byte)Enum.Parse(typeof(T), name);
            }
            catch
            {
                var intEnum = (int)Enum.Parse(typeof(T), name);
                result = (byte)intEnum;
            }

            return result;
        }
        public static int ConverToInt<T>(string name, int defaultValue = 0)
        {
            try
            {
                return (int)Enum.Parse(typeof(T), name);
            }
            catch { }

            return defaultValue;
        }
        public static string GetKeyName<T>(int value)
        {
            var name = "";
            try
            {
                name = Enum.GetName(typeof(T), value);
            }
            catch
            {
                name = "";
            }
            return name;
        }
        public static string ToIntegerString(this Enum enVal)
        {
            return Convert.ToInt32(enVal).ToString();
        }
        public static int ToInt(this Enum enVal)
        {
            return Convert.ToInt32(enVal);
        }
        /// <summary>
        /// Gets an attribute on an enum field value
        /// </summary>
        /// <typeparam name="T">The type of the attribute you want to retrieve</typeparam>
        /// <param name="enumVal">The enum value</param>
        /// <returns>The attribute of type T that exists on the enum value</returns>
        /// <example>string desc = myEnumVariable.GetAttributeOfType<DescriptionAttribute>().Description;</example>
        public static T GetAttributeOfType<T>(this Enum enumVal) where T : System.Attribute
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
            return (attributes.Length > 0) ? (T)attributes[0] : null;
        }
    }
}
