﻿using ServiceStack.Data;
using ServiceStack.OrmLite;

namespace OtelzNewFeeds.Common.Factory
{
    public interface IMemoryOptimizedConnectionFactory : IDbConnectionFactory
    {
    }

    public class MemoryOptimizedConnectionFactory : OrmLiteConnectionFactory, IMemoryOptimizedConnectionFactory
    {
        public MemoryOptimizedConnectionFactory(string connectionString, IOrmLiteDialectProvider dialectProvider) : base(connectionString, dialectProvider) { }
    }
}
