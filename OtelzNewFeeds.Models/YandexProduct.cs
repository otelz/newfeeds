﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Models
{
    public class YandexProduct
    {
        public string PropertyID { get; set; }
        public string PropertyName { get; set; }
        public string FinalURL { get; set; }
        public string ImageURL { get; set; }
        public string DestinationName { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public string StarRating { get; set; }
        public string Category { get; set; }
        public string Address { get; set; }
        public string Score { get; set; }
        public string MaxScore { get; set; }
        public string Facilities { get; set; }
    }

    public class YandexProducts_General
    {
        public int FacilityId { get; set; }
        public string FacilityName { get; set; }
        public string Description { get; set; }
        public string FinalUrlSuffix { get; set; }
        public string FinalURL { get; set; }
        public string ImageURL { get; set; }
        public string PhotoName { get; set; }
        public string PhotoDirectory { get; set; }
        public string DestinationName { get; set; }
        public string StarNumber { get; set; }
        public string Category { get; set; }
        public string ContextualKeywords { get; set; }
        public string AddressKeys { get; set; }
        public string Address { get; set; }
        public string Currency { get; set; }
    }
}
