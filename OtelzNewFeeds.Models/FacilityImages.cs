﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Models
{
    public class FacilityImages
    {
        public string ImageURL { get; set; }
        public int PropertyID { get; set; }
        public string PropertyName { get; set; }
    }

    public class FacilityImages_General
    {
        public int FacilityId { get; set; }
        public string FacilityName { get; set; }
        public string ImageURL { get; set; }
        public string PhotoName { get; set; }
        public string PhotoDirectory { get; set; }
    }
}
