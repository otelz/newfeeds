﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Models
{
    public class Facility
    {
        public int FacilityId { get; set; }
        public string GlobalName { get; set; }
        public byte PublishStatus { get; set; }
    }
}
