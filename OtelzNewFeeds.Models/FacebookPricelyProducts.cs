﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Models
{
    public class FacebookPricelyProducts
    {
        public int HotelId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }
        public string Addresss { get; set; }
        public string Neighborhood { get; set; }
        public string Description { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Brand { get; set; }
        public string CheckInDate { get; set; }
        public string Currency { get; set; }
        public int LengthOfStay { get; set; }
        public string BasePrice { get; set; }
        public string SalePrice { get; set; }
        public string Fee { get; set; }
        public string Tax { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
    }
    public class FacebookPricelyProducts_General
    {
        public int FacilityId { get; set; }
        public string FacilityName { get; set; }
        public string FinalUrlSuffix { get; set; }
        public string PhotoName { get; set; }
        public string PhotoDirectory { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string Neighborhood { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public DateTime CheckInDate { get; set; }       
        public string Currency { get; set; }
        public int LengthOfStay { get; set; }
        public string Brand { get; set; }
        public decimal Fee { get; set; }
        public decimal Tax { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string FacilityUrl { get; set; }
        
    }
    
}
