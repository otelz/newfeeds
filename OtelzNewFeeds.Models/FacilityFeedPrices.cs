﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Models
{
    public class FacilityFeedPrices
    {
        public int FacilityId { get; set; }
        public decimal SalePrice { get; set; }
        public decimal Price { get; set; }
        public decimal NetPrice { get; set; }
        public string Currency { get; set; }
    }
}
