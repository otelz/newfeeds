﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Models
{
    public class Route
    {
        public string PageURL { get; set; }
    }
    public class Route_General
    {
        public string Name { get; set; }
        public short RouteType { get; set; }
        public string PageURL { get; set; }
    }
}
