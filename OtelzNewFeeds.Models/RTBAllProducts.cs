﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Models
{
    public class RTBAllProducts
    {
        public int PropertyID { get; set; } // int, not null
        public string PropertyName { get; set; } // nvarchar(500), null
        public string FinalURL { get; set; } // nvarchar(276), null
        public string ImageURL { get; set; } // nvarchar(4000), null
        public string Star { get; set; } // nvarchar(50), null
        public string DestinationName { get; set; } // nvarchar(50), null
        public string Description { get; set; } // nvarchar(250), not null
        public string Price { get; set; } // varchar(16), null
        public string SalePrice { get; set; } // varchar(16), null
        public string Category { get; set; } // nvarchar(max), null
        public string ContextualKeywords { get; set; } // nvarchar(2114), null
        public string Adress { get; set; } // nvarchar(154), not null
    }

    public class RTBAllProducts_General
    {
        public int FacilityId { get; set; }
        public string FacilityName { get; set; }
        public string FinalUrlSuffix { get; set; }
        public string FinalURL { get; set; } 
        public string ImageURL { get; set; }
        public string PhotoName { get; set; }
        public string PhotoDirectory { get; set; }
        public string DestinationName { get; set; }
        public string StarNumber { get; set; }
        public string Category { get; set; }
        public string ContextualKeywords { get; set; }
        public string AddressKeys { get; set; }
        public string Address { get; set; }
        public string AttributeDescription { get; set; }
        public string Currency { get; set; }
    }
    public class RTBAllProducts_Prices
    {
        public int PropertyID { get; set; } // int, not null
        public string Price { get; set; } // varchar(16), null
        public string SalePrice { get; set; } // varchar(16), null
    }
}
