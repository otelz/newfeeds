﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtelzNewFeeds.Models
{
    public class Property
    {
        public string PageURL { get; set; }
        public string PropertyType { get; set; }
        public string Action { get; set; }
    }

    public class Property_General
    {
        public string Name { get; set; }
        public short RouteType { get; set; }
        public string PageURL { get; set; }
        public string Action { get; set; }
        public string PropertyType { get; set; }
    }


}
