using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Funq;
using ServiceStack;
using ServiceStack.Configuration;
using OtelzNewFeeds.ServiceInterface;
using System;
using System.Threading.Tasks;
using OtelzNewFeeds.Common.Constants;

namespace OtelzNewFeeds
{
    public class Startup : ModularStartup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public new void ConfigureServices(IServiceCollection services)
        {
            AppConstants config = new AppConstants();
            Configuration.Bind("AppConstants", config);
            services.AddSingleton(config);
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            string baseDir = env.ContentRootPath;
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Path.Combine(baseDir, "App_Data"));

            //if (env.IsDevelopment())
            //{
                app.UseDeveloperExceptionPage();
            //}

            //Register your ServiceStack AppHost as a .NET Core module
            app.UseServiceStack(new AppHost
            {
                AppSettings = new NetCoreAppSettings(Configuration) // Use **appsettings.json** and config sources
            });

            app.Run(context =>
            {
                context.Response.Redirect("/metadata");
                return Task.FromResult(0);
            });
        }
    }
    
}
