﻿using Funq;
using Microsoft.Extensions.Configuration;
using OtelzNewFeeds.Common.Factory;
using OtelzNewFeeds.Common.Helpers;
using OtelzNewFeeds.Models;
using OtelzNewFeeds.Operations;
using OtelzNewFeeds.ServiceInterface;
using OtelzNewFeeds.ServiceModel;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;

namespace OtelzNewFeeds
{
    public class AppHost : AppHostBase
    {
        public override ServiceStack.RouteAttribute[] GetRouteAttributes(Type requestType)
        {
            var routes = base.GetRouteAttributes(requestType);
            routes.Each(x => x.Path = x.Path.ToLower(CultureInfo.GetCultureInfo("en-US")));
            return routes;
        }
        public AppHost() : base("OtelzNewFeeds", typeof(FacebookFeedsService).Assembly) { }

        // Configure your AppHost with the necessary configuration and dependencies your App needs
        public override void Configure(Funq.Container container)
        {

            SetConfig(new HostConfig
            {
                //AppendUtf8CharsetOnContentTypes = new HashSet<string> { MimeTypes.XmlText },
                //StrictMode = true,
                //DebugMode = true,
                //DefaultContentType = MimeTypes.FormUrlEncoded,
                //SkipFormDataInCreatingRequest = false
            });
            //Plugins.Add(new CorsFeature(allowedOrigins: "*",
            //        allowedMethods: "GET, POST, OPTIONS",
            //        allowedHeaders: "Content-Type,OzApiKey",
            //        allowCredentials: false));
            //PreRequestFilters.Add((httpReq, httpRes) =>
            //{
            //    httpReq.UseBufferedStream = true;
            //});

            GlobalRequestFilters.Add((req, res, requestDto) =>
            {
                if ((requestDto is ResetFacilityFeedPrices || requestDto is ResetFacebookPricelyProducts || requestDto is ResetProductsRTB)
                     && !(req.AbsoluteUri.Contains("localhost") || req.RemoteIp == "::1"))
                {
                    res.StatusCode = (int)HttpStatusCode.Unauthorized;
                    res.StatusDescription = "You have no permission for this operation!";
                    res.EndHttpHandlerRequest(skipHeaders: true);
                    return;
                }
            });
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();

            //MiddleStage DB'si için
            IDbConnectionFactory mainDb = new OrmLiteConnectionFactory(configuration.GetConnectionString("DBOtelzMiddleStageConnection"), SqlServerDialect.Provider);
            container.Register(mainDb);


            //MemoryOptimized DB'si için
            IMemoryOptimizedConnectionFactory moDb = new MemoryOptimizedConnectionFactory(configuration.GetConnectionString("DBOtelzMemoryOptimizedConnection"), SqlServerDialect.Provider);
            container.Register(moDb);

            container.RegisterAs<FeedPriceOperations, IFeedPriceOperations>().ReusedWithin(ReuseScope.Request);
            container.RegisterAs<RTBOperations, IRTBOperations>().ReusedWithin(ReuseScope.Request);
            container.RegisterAs<FacebookOperations, IFacebookOperations>().ReusedWithin(ReuseScope.Request);
            container.RegisterAs<GoogleOperations, IGoogleOperations>().ReusedWithin(ReuseScope.Request);
            container.RegisterAs<YoutubeOperations, IYoutubeOperations>().ReusedWithin(ReuseScope.Request);
            container.RegisterAs<YandexOperations, IYandexOperations>().ReusedWithin(ReuseScope.Request);
            container.RegisterAs<CacheOperations, ICacheOperations>().ReusedWithin(ReuseScope.Request);
            container.RegisterAs<FacilityImageOperations, IFacilityImageOperations>().ReusedWithin(ReuseScope.Request);


            CsvSerializer.UseEncoding = PclExport.Instance.GetUTF8Encoding(true);
            this.ContentTypes.Register(CustomServiceStackXmlFormat.Format, CustomServiceStackXmlFormat.Serialize2, CustomServiceStackXmlFormat.Deserialize);

        }
    }
}
